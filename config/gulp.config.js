const src = "./src/" //源码根目录
const dist = "./dist/"  //开发和生产构建的根目录
const middle = "./middle/" //生产构建的中转站。只存在构建过程中
const md5 = './rev'//版本号临时存放目录。只存在构建过程中
module.exports = {
    devUrl: 'http://test.zhikaizaixian.com:8888',
    buildUrl: 'http://app.zhikaizaixian.com',
    src,
    dist,
    middle,
    md5,
    babelExclude: ['zepto.js'],
    compress: false,//生产环节是否开启代码压缩
    version: false,//是否开启版本控制
    cssMerge: 'app.min.css',
    jsMerge: ['zepto.js','util.js'],  //[*.*]合并所有   []不合并 vendor.js
    cssPreprocessor: 'sass', //css预编译 sass less postcss
    assets: { //其他资源目录
        src: src + "/assets/**/*",
        dist: dist + 'assets'
    },
    html: {
        src: src + "*.html",
        dist: dist
    },
    css: {
        src: src + "css/*.css",
        dist: dist + "css"
    },
    less: {
        src: src + "less/*.*",
        dist: dist + "css"
    },
    sass: {
        src: src + "sass/*.*",
        dist: dist + "css"
    },
    images: {
        src: src + "images/*.*",
        dist: dist + "images"
    },
    js: {
        src: src + "js/*.*",
        reserved: ['$', "exports", "module", "require", "define"],//混淆时忽略某个变量或语法
        dist: dist + "js"
    },
    font: {
        src: src + "font/*.*",
        dist: dist + "font"
    }
}