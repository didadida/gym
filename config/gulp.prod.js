const gulp = require('gulp')
const autoprefixer = require('gulp-autoprefixer')
const cssnano = require('gulp-cssnano')
const sass = require('gulp-sass')
const less = require('gulp-less')
const uglify = require('gulp-uglify')
const concat = require('gulp-concat')
const imagemin = require('gulp-imagemin')
const del = require('del')
const runSequence = require('run-sequence')
const rev = require('gulp-rev');
const revCollector = require('gulp-rev-collector');
const babel = require('gulp-babel')
const gulpIf = require('gulp-if')
const replace = require('gulp-replace');

const config = require('./gulp.config')
const reg = /\/[^\/]+$/

module.exports = function () {
    gulp.task(config.cssPreprocessor, e => gulp
      .src(config[config.cssPreprocessor].src)
      .pipe(eval(config.cssPreprocessor)())
      .pipe(gulpIf(f => config.cssMerge, concat(config.cssMerge || 'app.min.css')))
      .pipe(gulpIf(config.compress, cssnano()))
      .pipe(gulpIf(f => config.version, rev()))
      .pipe(autoprefixer({browsers: ['safari 5', 'ios 6', 'android 4']}))
      .pipe(gulp.dest(config[config.cssPreprocessor].dist))
      .pipe(rev.manifest('css.json'))
      .pipe(gulp.dest(config.md5))
    )

    gulp.task('jsMerge', e => gulp
      .src(config.jsMerge.map(v => config.js.src.replace(reg, '/') + v))
      .pipe(gulpIf(file => config.babelExclude.find(v => v !== file.relative), babel({
          presets: ['env'],
      })))
      .pipe(concat('vendor.js'))
      .pipe(uglify({
          mangle: {reserved: config.js.reserved}
      }))
      .pipe(gulpIf(f => config.version, rev()))
      .pipe(replace(config.devUrl, config.buildUrl))
      .pipe(gulp.dest(config.js.dist))
      .pipe(rev.manifest('vendor.json'))
      .pipe(gulp.dest(config.md5))
    )
    gulp.task('js', e => gulp
      .src([config.js.src].concat(config.jsMerge.map(v => '!' + config.js.src.replace(reg, '/') + v)))
      .pipe(babel({
          presets: ['env'],
          plugins: ["transform-object-assign"]
      }))
      .pipe(gulpIf(f => config.compress, uglify({
          mangle: {reserved: config.js.reserved}
      })))
      .pipe(gulpIf(f => config.version, rev()))
      .pipe(replace(config.devUrl, config.buildUrl))
      .pipe(gulp.dest(config.js.dist))
      .pipe(rev.manifest('js.json'))
      .pipe(gulp.dest(config.md5))
    )
    gulp.task('images', e => gulp
      .src(config.images.src)
      .pipe(imagemin({
          optimizationLevel: 3
          , progressive: true
          , interlaced: true
      }))
      .pipe(gulp.dest(config.images.dist))
    );
    gulp.task('font', e => gulp
      .src(config.font.src)
      .pipe(gulp.dest(config.font.dist))
    );
    gulp.task('rev', e => gulp
      .src(['./rev/*.json', config.html.src])
      .pipe(revCollector({
          replaceReved: true
      }))
      .pipe(replace(config.devUrl, config.buildUrl))
      .pipe(gulp.dest(config.html.dist))
    );
    gulp.task('html', e => gulp
      .src(config.html.src)
      .pipe(gulp.dest(config.html.dist))
    );
    gulp.task('del', e => del([config.dist, config.md5]))
    gulp.task('entry', e => runSequence('del', config.cssPreprocessor, 'jsMerge', 'js', 'images', 'font', config.version ? 'rev' : 'html'));
}