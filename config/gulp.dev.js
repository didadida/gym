const gulp = require('gulp')
const plumber = require('gulp-plumber');
const sass = require('gulp-sass')
const less = require('gulp-less')
const gulpIf = require('gulp-if')
const concat = require('gulp-concat')
const notify = require('gulp-notify')
const browserSync = require('browser-sync').create()
const config = require('./gulp.config')
const reload = browserSync.reload;
const del = require('del')
const runSequence = require('run-sequence')
const mock = require('gulp-mock-server')
const reg = /\/[^\/]+$/
const css = config.cssPreprocessor
module.exports = function () {
    gulp.task('del', e => del([config.dist, config.middle, config.md5]))
    gulp.task(css, e => gulp
        .src(config[css].src)
        .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')}))
        .pipe(eval(css)())
        .pipe(gulpIf(f => config.cssMerge, concat(config.cssMerge || 'app.min.css')))
        .pipe(gulp.dest(config[css].dist))
        .pipe(reload({stream: true}))
    )

    gulp.task('jsMerge', e => gulp
        .src(config.jsMerge.map(v => config.js.src.replace(reg, '/') + v))
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(config.js.dist))
        .pipe(reload({stream: true}))
    )

    gulp.task('js', e => gulp
        .src([config.js.src].concat(config.jsMerge.map(v => '!' + config.js.src.replace(reg, '/') + v)))
        .pipe(gulp.dest(config.js.dist))
        .pipe(reload({stream: true}))
    )

    gulp.task('html', e => gulp
        .src(config.html.src)
        .pipe(gulp.dest(config.html.dist))
        .pipe(reload({stream: true}))
    )
    gulp.task('font', e => gulp
        .src(config.font.src)
        .pipe(gulp.dest(config.font.dist))
        .pipe(reload({stream: true}))
    )
    gulp.task('images', e => gulp
        .src(config.images.src)
        .pipe(gulp.dest(config.images.dist))
        .pipe(reload({stream: true}))
    )
    gulp.task('mock', e => gulp
        .src('.')
        .pipe(mock({
            port: 10010,
            mockDir:config.src+'mock'
        }))
    )
    const arr = ['html', css, 'images', 'font']
    gulp.task('entry', e => {
        del(config.dist).then(rs => {
                runSequence(...(arr.concat('js', 'jsMerge')))
                browserSync.init({
                    server: {
                        baseDir: config.dist
                    }, notify: true
                });
                arr.forEach(v => gulp.watch(config[v].src, [v]))
                gulp.watch(config.js.src, ['js', 'jsMerge'])
            }
        );
    })
}