/* userClassRecord: {
    "id": 1,
    "title":"1VS1私教",//列表标题
    "coachName":"李思思", //教练名称
    "classTime": "2017-08-15 15:36:00", //上课时间
    "reserveTime": "2017-08-15 15:36:00", //预约时间
    "endTime": "", //确认上课时间
    "status": 0 //0预约中  1预约成功  2上课完成
}*/

module.exports = [
    {
        route:'/userClassRecord',
        response: './userClassRecord.json',
        delay:1000
    },
    {
        route:'/confirmTime',
        response: './confirmTime.json',
        delay:3000
    }
]