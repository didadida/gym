const filters = {
    leftNum: null,
    rightNum: null,
    sex: null,
    coachNeed: null,
    coachLevel: null,
    serveTime: null,
}
const action = window.action = {
    club: $('#md'),
    list: $('.ary'),
    condi: $('.condition'),
    condiType: $('.condition dl'),
    types: $('.condition h1'),
    ipts: $('input[type=number]'),
    pageNum: 1,
    index: 0,
    clubId: null,
    filters,
    toggleMask(flag, type) {
        if (flag) {
            //打开弹窗
            this.condi.show()
            this.condiType.hide()
            this.types.removeClass('act')
            this.condiType.eq(type).show()
            this.types.eq(type).addClass('act')
            this.index = type
        } else {
            //关闭弹窗
            this.condi.hide()
            if (!type) return
            let o = {}
            const condi = this.condiType.eq(this.index)
            this.condiType.eq(1 - this.index).find('.act').removeClass('act')
            condi.find('.act').each((_, v) => o[v.dataset.key] = v.dataset.value)
            this.index == 0 && this.ipts.each((_, v) => v.value && (o[v.name] = v.value))
            this.filters = Object.assign({}, filters, o)
            this.fetch()
        }
    },
    clearAct() {
        $('.info .act').removeClass('act')
    },
    clearInput() {
        this.ipts.val('')
    },
    reset() {
        this.clearInput()
        this.clearAct()
    },
    renderItem(v) {
        return `
            <a href="/coachDetails.html?coachId=${v.id}" class="flex p10">
                <div class="img-box"><img src="${v.userImageDto}"></div>
                <dl class="pl10 pr10">
                    <dt>${v.accountNameDto}</dt>
                    <dd class="star${v.stars} iconfont"></dd>
                    <dd>${v.introductionDto}</dd>
                </dl>
                <p class="ksprice"><span>${v.priceDto || 0}</span></p>
            </a>
        `
    },
    fetch(more) {
        if (!more) {
            this.pageNum = 1
            this.list.html(' ')
            $.loading()
        }
        const self = this
        this.xhr = $.fetch({
            url: 'coachlist',
            // type:'post',
            data: Object.assign({
                clubId: this.clubId,
                pageSize: $.globalConfig.pageSize,
                pageNum: this.pageNum,
                popular: 0
            }, this.filters),
            cb(rs) {
                $.hide()
                if (more) {
                    self.loadMoreOption.el.hide()
                } else {
                    self.list.html('')
                    $(window).scrollTop(0)
                }
                self.loadMoreOption.allow = rs.page * rs.limit < rs.total
                if (rs.rows.length === 0) return
                rs.rows = rs.rows.concat(rs.rows, rs.rows, rs.rows)
                self.pageNum++
                self.list.append(rs.rows.map(v => self.renderItem(v)).join(""))
            }
        })
    },
    loadMoreOption: {
        allow: false,
        el: $(".loadingMore"),
        cb() {
            action.fetch(1)
        }
    },
    selectMD() {
        this.clubId = this.club.val()
        this.fetch()
    },
    init() {
        this.selectMD()
        $.loadMore(this.loadMoreOption)
        const that = this
        $('.info').on('click', 'p', (function () {
            that.clearInput()
            const self = $(this)
            if (self.hasClass('act')) return self.removeClass('act')
            self.addClass('act').siblings().removeClass('act')
        }))
    }
}
action.init()