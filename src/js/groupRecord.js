$.loading()
const action = window.action = {
    el: $('.record ul'),
    pageNum: 1,
    renderStatus(v, flag) {
        switch (v.status) {
            case 0:
                return flag ? `<a href="cancelGroupCourseUI?id=${v.id}">取消预约</a>` : ''
            case 1:
                if(v.cmstatus!=null){
                    //0未评价1已评价
                    if(v.cmstatus==0){
                        return `<a href="commentUI?id=${v.id}&courseType=2&courseName=${v.courseNameDto}">评价</a>`
                    }else{
                        return `已评价`
                    }
                }else{
                    return `<a href="commentUI?id=${v.id}&courseType=2&courseName=${v.courseNameDto}">评价</a>`
                }


            default:
                return ''
        }
    },
    renderItem(v) {
        const flag = Date.now() < new Date(v.courseStartDto.replace(/-/g, '/')).getTime()
        return `<li>
                <h1 class="p10 flex between">
                    <b>${v.courseNameDto}</b>
                </h1>
                <div class="p10">
                    <div class="flex">
                        <p class="flex1">上课教练：<span>${v.coachnameDto}</span></p>
                        <p class="flex1">上课状态：<span>${v.status == 0 && !flag ? '待确认上课' : v.parStatusDto}</span></p>
                    </div>
                    <p>上课时间：<span>${v.courseStartDto || '无'}</span></p>
                    <p>预约时间：<span>${v.createtime || '无'}</span></p>
                    <p>确认上课时间：<span>${v.endtime || '无'}</span></p>
                    <div class="flex">
                        <p class="flex1">价格：<span>${v.priceDto}</span></p>
                        <p class="flex1">上课人数：<span>${v.currentPsNumDto + '/' + v.personNumDto}</span></p>
                    </div>
                    <aside>${this.renderStatus(v, flag)}</aside>
                </div>
            </li>`.replace(/\s+/g, ' ')
    },
    get (o = {}) {
        const self = this
        $.fetch({
            type: 'get',
            url: 'groupClassRecord',
            data: {
                pageNum: this.pageNum,
                pageSize: $.globalConfig.pageSize
            },
            cb(rs) {
                //无数据的时候
                if(rs.total==0){
                    $("#noneDiv").show();
                }
                $.hide()
                o.allow = true
                o.el && o.el.hide()
                self.pageNum++
                self.el.append(rs.rows.map(v => self.renderItem(v)).join(""))
                if (rs.total <= rs.limit * rs.page) {
                    window.onscroll = null
                }
            }
        })
    },
    init() {
        const self = this
        $.loadMore({
            allow: true,
            el: $(".loadingMore"),
            cb() {
                self.get(this)
            }
        })
        this.get()
    }
}
action.init()

