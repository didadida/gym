const action = window.action = {
    validDuration: 15 * 60 * 1000, //有效期15分钟
    endTime: 0,
    orderId: $.getUrlParams().orderId,
    time: null,
    timer: null,
    render(rs) {
        $(".data").html(`
            <div class="flex between bt10 p10">
                <div><h1>${common.statsuDto(rs.status)}</h1>${rs.status == 0&&rs.activityProductId ? '<span class="red iconfont icon-guanbi"><span class="time">14分59秒</span>后未支付，此订单将自动关闭</span>' : ''}</div>
                <img src="./images/${rs.status == 0 ? '6' : rs.status == 6 ? '4' : '5'}.png" style="height: auto">
            </div>
            <div class="bt10 p10">
                <div class="flex">
                    <img src="${rs.imageUrl}">
                    <div class="flex column between vstart pl10">
                        <h1>${rs.orderName}</h1>
                        <h2 class="grey">${rs.productType == 1 ? (rs.number + '课时') : ''}</h2>
                    </div>
                </div>
                <div class="flex between pt10">
                    <span class="grey">${rs.status == 0 ? '待付款' : rs.status == 6 ? '总金额' : '实付款'}</span>
                    <span class="red">￥${rs.price}</span>
                </div>
            </div>
            <div class="bt10 p10 grey">
                <div class="flex between">
                    <span>${rs.status == 0 ? '下单' : rs.status == 6 ? '取消' : '支付'}时间：</span>
                    <span>${rs.status == 0 ? rs.createTime : rs.status == 6 ? rs.cancelTime : rs.payTime}</span>
                </div>
                <div class="flex between">
                    <span>订单编号：</span>
                    <span>${rs.orderNo}</span>
                </div>
            </div>
            ${rs.status == 0 ? `
                <div class="control flex end">
                    <b class="grey" onclick="action.operating(this,${this.orderId},0)">取消订单</b>
                    <b class="red" onclick="action.operating(this,${this.orderId},1)">立即支付</b>
                </div>
            ` : ''}
        `)
    },
    padleft(n) {
        return (n < 10 ? '0' : '') + n
    },
    cutDown() {
        if (this.endTime <= Date.now()) {
            this.time.parent().html("订单已关闭")
            $(".control").remove()
            clearTimeout(this.timer)
        } else {
            const t = parseInt((this.endTime - Date.now()) / 1000)
            const m = parseInt(t / 60)
            const s = parseInt(t % 60)
            this.time.html(this.padleft(m) + "分" + this.padleft(s) + "秒")
            this.timer = setTimeout(() => this.cutDown(), 1000)
        }
    },
    operating(dom, id, flag) {
        common.operating(dom, id, flag, (rs) => {
            location.reload()
        })
    },
    fetch() {
        $.loading()
        const self = this
        this.xhr = $.fetch({
            type: 'get',
            url: '/ordersDetails',
            data: {
                orderId: this.orderId
            },
            cb(rs) {
                $.hide()
                rs = rs || {}
                self.render(rs)
                if (rs.status == 0&&rs.activityProductId) {
                    self.time = $(".time")
                    self.endTime = new Date(rs.createTime.replace(/-/g, '/')).getTime() + self.validDuration
                    self.cutDown()
                }
            }
        })
    },
    init() {
        this.fetch()
    }
}
action.init()