// window.baseUrl = 'http://qa.lailaiche.com:8080/fitness/wx/'
window.baseUrl = 'http://39.105.97.253:8080/fitness/wx/'
const prevent = e => (e || window.event).preventDefault()
var base = {
    el: null,
    show: function (opts) {
        if (this.el) {
            return this.update(this.el, opts);
        }
        var el = this.el = $('<div class="flex loading"></div>');
        setTimeout(e => {
            el.addClass('show')
        }, 30)
        this.update(el, opts);
        $(document.body).append(el);
        this.el.on('touchmove', prevent, false)
    },
    bindEvt(opts) {
        this.unbind()
        setTimeout(function () {
            $('.c-ok').on('click', opts.ok || false);
            $('.c-cancel').on('click', opts.cancel || this.hide.bind(this))
        }.bind(this), 0)
    },
    unbind() {
        const ok = $('.ok')
        const cancel = $('.cancel')
        ok.length && ok.off('click');
        cancel.length && cancel.off('click')
        this.el && this.el.off('touchmove', prevent, false)
    },
    hide: function () {
        this.unbind()
        if (this.el) {
            $(this.el).removeClass('show');
            setTimeout(() => {
                this.el.remove();
                this.el = null
            }, 200)
        }
        document.ontouchmove = null
    },
    update: function (el, opts) {
        var htm = '<div class="loading-cnt">';
        if (opts.type === 1) {
            var info = opts.msg ? '<div class="info">' + opts.msg + '</div>' : ''
            htm += '<h4>' + opts.text + '</h4>' + info +
              '<h3 class="flex around">' + '<a class="c-cancel">取消</a>' +
              '<a class="c-ok">' + opts.okText + '</a>' +
              '</h3>'
        } else if (opts.type === 2) {
            htm += '<h6 class="iconfont ' + opts.icon + '"></h6><div>' + opts.text + '</div><h5 onclick="wating()" class="c-comfrim">确定</h5>'
        } else {
            htm += '<h6 class="iconfont ' + opts.icon + '"></h6><div>' + opts.text + '</div>'
        }
        htm += '</div>';
        el.html(htm)
        this.bindEvt(opts)
    }
};

$.loading = function (text) {
    base.show({
        icon: 'icon-loading',
        text: (text || '正在加载') + '...'
    })
};
$.success = function (text, time, fn, icon) {
    base.show({
        icon: icon || 'icon-chenggong',
        text: text || '操作成功!'
    })
    setTimeout(function () {
        base.hide()
        fn && fn()
    }, time || 3000)
};
$.confirm = function (o) {
    o = o || {}
    base.show({
        type: 1,
        icon: 'icon-jinggao',
        text: o.title || '是否执行该操作',
        ok: o.ok,
        cancel: o.cancel,
        okText: o.okText || '确定',
        msg: o.msg
    })
};
$.error = function (text, time, fn) {
    $.success(text || '操作失败', time || 2500, fn, 'icon-jinggao')
    return false
}
$.wating = function (text, fn) {
    window.wating = function () {
        fn && fn()
        window.wating = null
    }
    base.show({
        icon: 'icon-jinggao',
        text: text,
        type: 2
    })
}

$.hide = base.hide.bind(base)
$.fetch = function (o = {}) {
    $.ajax({
        timeout: 20000,
        type: o.type || 'GET',
        url: baseUrl + o.url,
        data: o.data || {},
        dataType: 'json',
        contentType: "application/json",
        success(data, status, xhr) {
            if (data.code === 200) {
                o.cb && o.cb(data.data)
            } else if (o.cb_error) {
                o.cb_error(data)
            } else {
                $.error(data.msg)
            }
        },
        error(xhr, errorType, error) {
            console.error(error)
            if (errorType === 'timeout') return $.confirm({
                title: '请求失败，是否重试？',
                ok: function () {
                    $.loading('正在重新请求')
                    $.fetch(o)
                }
            })
            $.error('网络错误！')
        }
    })
}

$.getUrlParams = function () {
    let params = {}
    const _params = window.location.search.replace('?', '').split('&').forEach(v => {
        const kv = v.split('=')
        params[kv[0]] = kv[1]
    })
    return params
}

$.loadMore = (o = {}) => {
    window.onscroll = function () {
        const st = document.body.scrollTop || document.documentElement.scrollTop
        const sh = document.body.scrollHeight || document.documentElement.scrollHeight
        const winh = window.innerHeight
        const disY = sh - st - winh
        if (disY < (o.distance || 20) && o.allow) {
            o.el && o.el.show()
            o.allow = false
            o.cb && o.cb(disY)
        }
    }
}
$.globalConfig = {
    pageSize: 20
}