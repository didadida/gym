const common = {
    statsuDto(status) {
        switch (+status) {
            case 0:
                return `待支付`
          // case 1:
          // case 3:
          // case 4:
          //     return `支付成功`
          // case 2:
          //     return `交易失败关闭`
          // case 5:
          //     return `定金支付`
            case 6:
                return `已取消`
          // case 7:
          //     return `已退款`
            default:
                return `已完成`
        }
    },
    operating(dom, id, flag, cb) {
        //取消订单
        const k = flag ? '支付' : '取消'
        const url = flag ? '支付地址' : 'wxcancelOrder'//替换地址
        $.loading("正在" + k + "订单")
        const self = this
        $.fetch({
            url,
            data: {
                orderId: id
            },
            cb(rs) {
                $.success("订单" + k + "成功", null, function () {
                    cb(rs)
                })
            }
        })
    }
}