$.loading()
const action = window.action = {
    el: $('.record ul'),
    pageNum: 1,
    renderStatus(v) {
        debugger;
        switch (v.parStatusDto) {
            case '待确认上课':
                return `<a onclick="action.confirm(this,${v.id})">确认上课</a>`
            case '预约中':
                return `<a href="trainPlanUI?id=${v.id}">训练计划</a>`
            case '已上课':
                if(v.cmstatus!=null){
                    //0未评价1已评价
                    if(v.cmstatus==0){
                        return `<a href="trainContentUI?id=${v.id}">训练内容</a><a href="commentUI?id=${v.id}&courseType=1&courseName=${v.courseNameDto}">评价</a>`
                    }else {
                        return `<a href="trainContentUI?id=${v.id}">训练内容</a> 已评价`
                    }
                }else {
                    return `<a href="trainContentUI?id=${v.id}">训练内容</a><a href="commentUI?id=${v.id}&courseType=1&courseName=${v.courseNameDto}">评价</a>`
                }
            default:
                return ''
        }
    },
    renderItem(v) {
        return `
            <li>
                <h1 class="p10 flex between">
                    <b>${v.courseNameDto}</b>
                </h1>
                <div class="p10">
                    <div class="flex">
                        <p class="flex1">上课教练：<span>${v.coachnameDto}</span></p>
                        <p class="flex1">上课状态：<span${v.status === 1 ? ' class="ok"' : ''}>${v.parStatusDto}</span></p>
                    </div>
                    <p>开始时间：<span>${v.courseStartDto || '无'}</span></p>
                    <p>结束时间：<span>${v.courseEndDto || '无'}</span></p>
                    <p>预约时间：<span>${v.createtime || '无'}</span></p>
                    <p>确认上课时间：<span>${v.endtime || '无'}</span></p>
                    <aside>${this.renderStatus(v)}</aside>
                </div>
            </li>
        `.replace(/\s+/g, ' ')
    },
    get (o = {}) {
        const self = this
        $.fetch({
            type: 'get',
            url: 'classRecord',
            data: {
                pageNum: this.pageNum,
                pageSize: 10
            },
            cb(rs) {
                //无数据的时候
                if(rs.total==0){
                    $("#noneDiv").show();
                }
                $.hide()
                o.allow = true
                o.el && o.el.hide()
                self.pageNum++
                self.el.append(rs.rows.map(v => {
                    // console.error(self.renderItem(v))
                    return self.renderItem(v)
                }).join(""))
                if (rs.total <= rs.limit * rs.page) {
                    window.onscroll = null
                }
            }
        })
    },
    confirm(self, id) {
        const that = this
        $.confirm({
            title: '是否确认上课?',
            ok() {
                $.fetch({
                    url: 'cfmCourse',
                    data: {id},
                    cb(rs) {
                        $.success('确认上课成功', null, function () {
                            $(self).parents('li').replaceWith(that.renderItem(rs))
                        })
                    }
                })
            }
        })
    },
    init() {
        const self = this
        $.loadMore({
            allow: true,
            el: $(".loadingMore"),
            cb() {
                self.get(this)
            }
        })
        this.get()
    }
}
action.init()

