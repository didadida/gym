$.loading()
const action = window.action = {
    el: $('.record ul'),
    pageNum: 1,
    get (o = {}) {
        const self = this
        $.fetch({
            type: 'get',
            url: 'admissionRecordPage',
            data: {
                pageNum: this.pageNum,
                pageSize: 10
            },
            cb(rs) {
                //无数据的时候
                if(rs.total==0){
                    $("#noneDiv").show();
                }
                $.hide()
                o.allow = true
                o.el && o.el.hide()
                self.pageNum++
                self.el.append(rs.rows.map(v => `
                    <li class="pb10 pt10">
                        <p>入场时间：<span>${v.beginTime}</span></p>
                        <p>离场时间：<span>${v.endTime||'暂未离场'}</span></p>
                        <div class="flex">
                            <p class="flex1">手牌号：<span>${v.machineNo || '无'}</span></p>
                            <p class="flex1">入场状态：<span class="${v.status === 1 || v.status === 3 ? 'error' : 'ok'}">${v.status === 1 ? '签到中' : v.status === 2 ? '签到成功' : v.status === 1 ? '签退中' : '签退成功'}</span></p>
                        </div>
                    </li>
                `.replace(/\s+/g,' ')))
                if (rs.total <= rs.limit * rs.page) {
                    window.onscroll = null
                }
            }
        })
    },
    init() {
        const self = this
        $.loadMore({
            allow: true,
            el: $(".loadingMore"),
            cb() {
                self.get(this)
            }
        })
        this.get()
    }
}
action.init()


