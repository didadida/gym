!function(t) {
    function e(o) {
        if (n[o])
            return n[o].exports;
        var i = n[o] = {
            i: o,
            l: !1,
            exports: {}
        };
        return t[o].call(i.exports, i, i.exports, e),
          i.l = !0,
          i.exports
    }
    var n = {};
    e.m = t,
      e.c = n,
      e.i = function(t) {
          return t
      }
      ,
      e.d = function(t, n, o) {
          e.o(t, n) || Object.defineProperty(t, n, {
              configurable: !1,
              enumerable: !0,
              get: o
          })
      }
      ,
      e.n = function(t) {
          var n = t && t.__esModule ? function() {
                return t.default
            }
            : function() {
                return t
            }
          ;
          return e.d(n, "a", n),
            n
      }
      ,
      e.o = function(t, e) {
          return Object.prototype.hasOwnProperty.call(t, e)
      }
      ,
      e.p = "",
      e(e.s = 2)
}([function(t, e) {}
    , , function(t, e, n) {
        "use strict";
        n(0),
          function(t) {
              function e(t, e) {
                  for (; ("" + t).length < (e || 2); )
                      t = "0" + t;
                  return "" + t
              }
              function n(e, n) {
                  return !(t(window).height() - e.get(0).getBoundingClientRect().bottom < n + 10)
              }
              function o() {
                  var e = t('<div class="pop-mask">');
                  t(".pop-mask").remove(),
                    e.appendTo("body")
              }
              function i(e) {
                  o(),
                    setTimeout(function() {
                        t(".pop-mask").addClass(e ? "pop-mask-in pop-mask-transparent" : "pop-mask-in")
                    }, 0)
              }
              function a(e) {
                  t(".pop-mask").removeClass("pop-mask-in pop-mask-transparent"),
                    setTimeout(function() {
                        t(".pop-mask").remove()
                    }, e ? 0 : 300)
              }
              function r(t, e) {
                  var n = 0
                    , o = 0;
                  return t /= 1,
                    e /= 1,
                    n = new Date(t + "/" + parseInt(e) + "/1"),
                  12 === e && (t += 1,
                    e = 0),
                    o = new Date(t + "/" + (parseInt(e) + 1) + "/1"),
                  parseInt(o - n) / 864e5
              }
              function s(t) {
                  var n = {
                      year: 30,
                      month: 12,
                      day: 31,
                      hour: 23,
                      minute: 59
                  }
                    , o = []
                    , i = 0;
                  for ("month" !== t && "day" !== t || (i = 1),
                         i; i < n[t] + 1; i++)
                      o.push(("year" === t ? "20" : "") + e(i));
                  return o
              }
              function c() {
                  var e = t('<div class="date-picker2"/>');
                  return e.html('<div class="date-picker2-toolbar">\n        <a class="cancel" href="javascript:;">取消</a>\n        <a class="confirm" href="javascript:;">确认</a>\n      </div>\n      <div class="date-picker2-title">\n        <div class="col-group"></div>\n      </div>\n      <div class="date-picker2-content">\n        <div class="col-group"></div>\n        <div class="picker-highlight"></div>\n      </div>').appendTo(document.body),
                    e
              }
              function l(e, n) {
                  var o = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {
                      type: "date",
                      date: k.now.YYYY + "-" + k.now.MM + "-" + k.now.DD + " " + k.now.hh + ":" + k.now.mm,
                      yearCols: v.yearCols,
                      monthCols: v.monthCols,
                      dayCols: v.dayCols,
                      hourCols: v.hourCols,
                      minuteCols: v.minuteCols,
                      contentPickerItemH: v.contentPickerItemH,
                      contentPickerItemShowNum: v.contentPickerItemShowNum,
                      titleDisplay: !0,
                      callback: function() {}
                  };
                  this.input = e,
                    this.opts = t.extend({}, o, n),
                    this.showPicker()
              }
              var u = /^\d{4}(-\d{2}){2}( \d{2}:\d{2})?$/
                , d = function() {
                  return !!("ontouchstart"in window || window.DocumentTouch && document instanceof DocumentTouch)
              }()
                , p = d ? "touchstart" : "mousedown"
                , h = d ? "touchmove" : "mousemove"
                , f = d ? "touchend" : "mouseup"
                , m = new Date
                , v = {
                  id: "",
                  display: !1,
                  datePickerEleClass: ".date-picker2",
                  contentColClass: ".col",
                  contentPickerItemH: 40,
                  contentPickerItemShowNum: 5,
                  dateFormatStr: "YYYY-MM-DD-hh-mm",
                  yearCols: s("year"),
                  monthCols: s("month"),
                  dayCols: s("day"),
                  hourCols: s("hour"),
                  minuteCols: s("minute")
              }
                , k = {
                  now: {
                      YYYY: e(m.getFullYear(), 4),
                      MM: e(m.getMonth() + 1),
                      DD: e(m.getDate()),
                      hh: e(m.getHours()),
                      mm: e(m.getMinutes())
                  },
                  select: {
                      YYYY: e(m.getFullYear(), 4),
                      MM: e(m.getMonth() + 1),
                      DD: e(m.getDate()),
                      hh: e(m.getHours()),
                      mm: e(m.getMinutes())
                  }
              }
                , g = {
                  start: 0,
                  offset: 0,
                  end: 0,
                  eleOffsetY: 0,
                  doing: !1
              };
              t.extend(l.prototype, {
                  showPicker: function() {
                      var e = this
                        , o = this
                        , a = o.input
                        , r = a.data("id")
                        , s = ""
                        , l = void 0
                        , d = o.opts
                        , m = d.type
                        , k = d.titleDisplay
                        , g = d.yearCols
                        , Y = d.monthCols
                        , C = d.dayCols
                        , P = d.hourCols
                        , y = d.minuteCols
                        , M = d.date
                        , w = "col-" + ("date" === m ? "33" : "20")
                        , D = new Date / 1
                        , I = v.dateFormatStr.split("-")
                        , b = [g, Y, C, P, y]
                        , S = []
                        , T = [];
                      if (o.opts.contentPickerItemShowNum % 2 == 0)
                          return alert("显示行数必须为奇数！");
                      if (i(),
                        s = a.val() && u.test(a.val()) ? a.val() : M,
                        S = s.split(" "),
                        T = S[1] ? S[0].split("-").concat(S[1].split(":")) : S[0].split("-"),
                        this.setSelectDate(T),
                        v.display) {
                          if (r === v.id)
                              return;
                          !0,
                            o.hidePicker(t(v.datePickerEleClass)),
                            l = c()
                      } else
                          l = c();
                      v.display = !0,
                        v.id = D,
                        a.data("id", D).addClass("focus"),
                      "date" === m && (I.length = b.length = T.length = 3),
                      k && o.createPickerTitleCol(l, w, I),
                        o.createPickerContentCol(l, w, I, b),
                        t.each(T, function(t, e) {
                            o.setPickerGroupPos(l, t, e, o)
                        }),
                        l.removeClass("top bottom").addClass(n(a, 196) ? "bottom" : "top").data("id", D).on(p, function() {
                            return !1
                        }),
                        setTimeout(function() {
                            l.addClass("in transition"),
                              t(document).off(),
                              t(document).on(p, function() {
                                  o.hidePicker(l)
                              })
                        }, 10);
                      var x = void 0;
                      l.find(v.contentColClass).on(p, function(e) {
                          x = t(this),
                            o.handleStart(e, x),
                            t(document).off(),
                            t(document).on(p, function() {
                                o.hidePicker(l)
                            }),
                            t(document).on(h, function(t) {
                                o.handleMove(t, x)
                            }),
                            t(document).on(f, function(t) {
                                o.handleEnd(t, x)
                            })
                      });
                      var H = l.find(".cancel")
                        , N = l.find(".confirm");
                      H.on(p, function() {
                          e.hidePicker(l)
                      }),
                        N.on(p, function() {
                            e.setInputVal(!0),
                              e.hidePicker(l)
                        })
                  },
                  hidePicker: function(e) {
                      v.display = !1,
                        t('input[data-type="date-picker2"]').removeClass("focus"),
                        e.removeClass("in"),
                        a(),
                        setTimeout(function() {
                            e.remove()
                        }, 400)
                  },
                  createPickerTitleCol: function(e, n, o) {
                      var i = "";
                      t.each(o, function(t, e) {
                          i += '<div class="' + n + '"><strong>' + e + "</strong></div>"
                      }),
                        e.find(".date-picker2-title .col-group").html(i)
                  },
                  createPickerContentCol: function(e, n, o, i) {
                      var a = "";
                      t.each(i, function(e, i) {
                          a += '<div class="col ' + n + '" data-type="' + o[e] + '"><div class="picker-group">',
                            t.each(i, function(t, e) {
                                a += '<div class="picker-item">' + e + "</div>"
                            }),
                            a += "</div></div>"
                      }),
                        e.find(".date-picker2-content").height(this.opts.contentPickerItemShowNum * this.opts.contentPickerItemH).find(".col-group").html(a)
                  },
                  setPickerGroupPos: function(e, n, o) {
                      var i = this;
                      e.find(".col").eq(n).find(".picker-item").each(function(e, n) {
                          t(n).html() === o && i.setPickerGroupAttr(t(n).parent(), -(e - Math.floor(i.opts.contentPickerItemShowNum / 2)) * i.opts.contentPickerItemH)
                      })
                  },
                  setPickerGroupAttr: function(t, e) {
                      t.css({
                          "-webkit-transform": "translateY(" + e + "px)",
                          transform: "translateY(" + e + "px)"
                      }).data("offsetY", "" + e)
                  },
                  setInputVal: function(t) {
                      var e = this.input
                        , n = ""
                        , o = this.opts
                        , i = o.date
                        , a = o.type;
                      if (t)
                          n = k.select.YYYY + "-" + k.select.MM + "-" + k.select.DD,
                          "date-time" === a && (n += " " + k.select.hh + ":" + k.select.mm+":00"),
                            e.val(n),
                            this.opts.callback(n);
                      else {
                          n = e.val() && u.test(e.val()) ? e.val() : i;
                          var r = n.split(" ");
                          "date" === a ? (n = n.replace(/\s.+/g, ""),
                            r = r[0].split("-")) : r = r[0].split("-").concat(r[1].split(":")),
                            this.setSelectDate(r)
                      }
                  },
                  setSelectDate: function(t) {
                      k.select.YYYY = t[0],
                        k.select.MM = t[1],
                        k.select.DD = t[2],
                        k.select.hh = t[3],
                        k.select.mm = t[4]
                  },
                  updateMonthDays: function(e, n) {
                      var o = r(e, n)
                        , i = t(".date-picker2 .col").eq(2)
                        , a = i.find(".picker-group")
                        , s = i.find(".picker-item")
                        , c = -(o - (Math.floor(this.opts.contentPickerItemShowNum / 2) + 1)) * this.opts.contentPickerItemH
                        , l = a.data("offset-y")
                        , u = Math.abs(o - s.length);
                      if (o > s.length)
                          for (var d = 0; d < u; d++) {
                              var p = t('<div class="picker-item" />');
                              p.html(s.length + 1 + d).appendTo(a)
                          }
                      else if (o < s.length) {
                          for (var h = 0; h < u; h++)
                              a.find(".picker-item").eq(-1).remove();
                          l < c && (this.setPickerGroupAttr(a, c),
                            k.select.DD = o)
                      }
                  },
                  handleStart: function(t, e) {
                      d || (g.doing = !0);
                      var n = e.find(".picker-group");
                      g.start = d ? t.targetTouches[0].pageY : t.clientY,
                        g.eleOffsetY = n.data("offset-y") || 0
                  },
                  handleMove: function(t, e) {
                      if (e && (d || g.doing)) {
                          var n = e.find(".picker-group");
                          g.offset = (d ? t.targetTouches[0].pageY : t.clientY) - g.start;
                          var o = g.eleOffsetY + g.offset;
                          this.setPickerGroupAttr(n, o)
                      }
                  },
                  handleEnd: function(t, e) {
                      if (e) {
                          var n = e.find(".picker-group")
                            , o = e.find(".picker-item")
                            , i = n.data("offset-y") || 0
                            , a = this.opts
                            , r = a.contentPickerItemH
                            , s = a.contentPickerItemShowNum
                            , c = Math.floor(s / 2) * r
                            , l = -(o.length - (Math.floor(s / 2) + 1)) * r
                            , u = 0
                            , d = e.data("type");
                          i >= c ? (u = 0,
                            i = c) : i <= l ? (u = o.length - 1,
                            i = l) : i >= 0 ? (u = Math.floor(s / 2) - Math.round(Math.abs(i) / r),
                            i = (Math.floor(s / 2) - u) * r) : (u = Math.round(Math.abs(i) / r) + Math.floor(s / 2),
                            i = -(u - Math.floor(s / 2)) * r),
                            n.addClass("transition"),
                            setTimeout(function() {
                                n.removeClass("transition")
                            }, 400),
                            this.setPickerGroupAttr(n, i),
                            k.select["" + d] = o.eq(u).html(),
                            g.doing = !1,
                          "YYYY" !== d && "MM" !== d || this.updateMonthDays(k.select.YYYY, k.select.MM)
                      }
                  }
              }),
                t.fn.datePicker = function(e) {
                    return t(this).attr({
                        readonly: "readonly",
                        "data-type": "date-picker2"
                    }),
                      t(this).on(p, function() {
                          return new l(t(this),e),
                            !1
                      })
                }
          }(Zepto)
    }
]);
