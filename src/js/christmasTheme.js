const toast = window.toast = {
    el: $(".toast"),
    items: $(".toast-inner>div"),
    showClass: "show",
    hideClass: "none",
    toggle(flag, index) {
        this.el.removeClass(flag ? this.hideClass : this.showClass)
        setTimeout(()=>{
            this.el.addClass(flag ? this.showClass : this.hideClass)
        },0)
        this.items.removeClass("show").eq(index).addClass(this.showClass)
    },
    success() {
        this.toggle(1, 0)
    },
    fail() {
        this.toggle(1, 1)
    },
    payfail() {
        this.toggle(1, 2)
    },
    close(index) {
        action.orderId = null;
        this.toggle(0, index)
    }
}
const action = window.action = {
    wrapper: $("footer"),
    times: $(".time>span"),
    state: $(".time>i"),
    timer: null,
    go: $(".go"),
    side: $("aside"),
    title: $(".title"),
    list: $(".items"),
    data: {starttime: '', endtime: ''},
    activityProductId: null,
    orderId: null,
    padleft(n) {
        return (n < 10 ? '0' : '') + n
    },
    loop(startTime, endTime) {
        const now = Date.now()
        if (endTime < now) {
            this.wrapper.addClass("over")
            clearTimeout(this.timer)
        } else {
            let cur
            if (startTime > now) {
                //还未开始
                // this.state.html("开始")
                this.wrapper.addClass("wait")
                cur = startTime
            } else if (endTime >= now) {
                //正在抢购
                this.wrapper.removeClass("wait")
                // this.state.html("结束")
                cur = endTime
            }

            const t = parseInt(Math.abs((cur - now)) / 1000)
            const h = parseInt(t / 60 / 60)
            const m = parseInt(t / 60 % 60)
            const s = parseInt(t % 60)

            this.times.html(this.padleft(h) + ":" + this.padleft(m) + ":" + this.padleft(s))
            this.timer = setTimeout(() => {
                this.cutDown()
            }, 1000)
        }
    },
    cutDown() {
        if (!this.data.endtime || !this.data.starttime) return;
        const endTime = new Date(this.data.endtime.replace(/-/g, '/')).getTime()
        const startTime = new Date(this.data.starttime.replace(/-/g, '/')).getTime()
        this.loop(startTime, endTime)
    },
    renderTableRow(v) {
        return `<tr><td>${v.purchaseNum}节</td><td>￥${v.price}</td><td>${v.totalNum}人</td><td>${v.leftNum}人</td></tr>`
    },
    renderPackageItem(v, index) {
        return `<dd onclick="action.chooseItem(this,${index},${v.activityProductId})">${v.purchaseNum}节</dd>`
    },
    renderInfo(price, surplus) {
        this.title.html(`<h4>￥${price}</h4><h5>库存：${surplus}件</h5>`)
        if (!surplus) {
            this.list.addClass("nomore")
        } else {
            this.list.removeClass("nomore")
        }
    },
    render() {
        let s1 = ''
        let s2 = ''
        let data = this.data.rows || []
        data.forEach((v, i) => {
            s1 += this.renderTableRow(v)
            s2 += this.renderPackageItem(v, i)
        })
        $("main>h1").html(`
            开始时间：${this.data.starttime.replace("-", "年").replace("-", "月").replace(" ", "日 ")}<br>
            结束时间：${this.data.endtime.replace("-", "年").replace("-", "月").replace(" ", "日 ")}
        `)
        $("tbody").html(s1 || "<tr><td colspan='4'>暂无数据</td></tr>")
        this.list.html(s2 || "")

        if (!this.data.status) {
            this.wrapper.addClass("over")
            return
        }
        let p = data.map(v => v.price).sort((a,b)=>b-a)
        console.error(p)
        const i = p.length ? (p[p.length - 1] + '-' + p[0]) : 0
        const j = data.reduce((sum,v)=>sum+v.leftNum,0)
        this.renderInfo(i, j)
    },
    toggle(flag) {
        this.side[flag ? 'addClass' : 'removeClass']("show")
    },
    chooseItem(dom, index, activityProductId) {
        console.error(activityProductId)
        this.activityProductId = activityProductId
        $(dom).addClass("active").siblings().removeClass("active");
        const item = this.data.rows[index]
        this.renderInfo(item.price, item.leftNum)
    },
    pay() {
        //购买
        if (!this.activityProductId) {
            return $.error("未选择商品")
        }
        const self = this
        toast.close()
        data = {
            activityProductId: this.activityProductId
        }
        if (this.orderId) {
            data = {
                orderId: this.orderId
            }
        }
        $.loading("正在支付")
        $.fetch({
            type: 'get',
            url: 'actprolist',
            data: data,
            cb(rs) {
                //购买成功应该返回orderId,已方便用户前往详情页面
                self.orderId = rs.orderId
                $.success('购买成功')
                //支付成功
                toast.success()
                self.toggle(0)
            },
            cb_error(rs) {
                //$.error('测试',1000) 第二个参数表示提示停留两秒
                // $.hide()
                //支付失败同样返回orderId,通过orderId重新支付，避免重新生成订单
                self.orderId = rs.orderId
                //支付失败
                // toast.payfail()
                //没有抢到
                // toast.fail()
                $.error('购买失败')
                self.toggle(0)
            }
        })
    },
    goto() {
        //购买成功查看订单详情
        if (!this.orderId) {
            return $.error("订单不存在")
        }
        window.location.href = "/myOrder.html?orderId=" + this.orderId
    },
    init() {
        const self = this
        $.loading()
        $.fetch({
            type: 'get',
            url: 'actprolist',
            data: {
                activityId: 1
            },
            cb(rs) {
                $.hide()
                $("main").show()
                rs && (self.data = rs)
                self.cutDown()
                self.render()
            }
        })
    }
}
action.init()
// toast.success()