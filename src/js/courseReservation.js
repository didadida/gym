const dayToCn = ['周日', '周一', '周二', '周三', '周四', '周五', '周六']
const action = window.action = {
    mask: $('dl'),
    list: $("ol"),
    types: $('h1'),
    clubId: null,//选择门店的id
    productId: null,//选择课程的id
    date: null,
    pageNum: 1,
    toggle(self, type) {
        if (type && !this.clubId) return $.error("请先选择门店");
        const _self = $(self)
        const that = this
        const targetMask = this.mask.eq(type)
        this.mask.removeClass('show')
        if (!_self.hasClass('act')) {
            _self.addClass('act')
            targetMask.addClass('show')
            _self.siblings().removeClass('act')
        } else {
            _self.removeClass('act')
        }
        if (targetMask.hasClass("init")) return;
        $.fetch({
            url: type ? 'prodDroplist/' : 'clubDroplist/',
            data: {
                clubId: this.clubId
            },
            cb(rs) {
                targetMask.addClass('init');
                const results = rs[type ? 'productlist' : 'clublist']
                if (results.length === 0) {
                    return targetMask.html(' <dt class="iconfont icon-favorite">此门店暂无课程</dt>')
                }
                targetMask.find("dd")
                  .replaceWith(results.map(v => `<dd class="iconfont" onclick="action.choose(this,${type},${v.id})">${v[type ? 'productName' : 'name']}</dd>`)
                    .join(""))
            }
        })
    },
    renderDays() {
        const now = Date.now()
        const days = [0, 1, 2, 3, 4, 5, 6].map(v => {
            const timeStamp = now + v * 24 * 60 * 60 * 1000
            const d = new Date(timeStamp)
            if (v === 0) {
                this.chooseDate(null, d.toJSON().substring(0, 10))
            }
            return `<li class="${v === 0 ? 'act' : null}" onclick="action.chooseDate(this,'${d.toJSON().substring(0, 10)}')"><h2>${v == 0 ? '今天' : v == 1 ? '明天' : v == 2 ? '后天' : d.toJSON().substring(5, 10)}</h2><h3>${dayToCn[d.getDay()]}</h3></li>`
        }).join("")
        $(".days7").html(days)
    },
    renderItem(v) {
        const status = v.seatStatusDto
        return `
               <li class="flex p10${status == 0 ? ' out' : status == 1 ? ' in' : ' full'}">
                <img src="${v.coachImageDto}">
                <div>
                    <h4>${v.courseName}</h4>
                    <h5>教练:${v.realname}</h5>
                    <h5>门店:${v.clubNameDto}</h5>
                    <h5>会员价:￥${v.vipPriceDto}  非会员价:￥${v.nonVipPriceDto || v.vipPriceDto}</h5>
                    <h6>${v.starttime.substring(10, 16)} -${v.endtime.substring(10, 16)}</h6>
                </div>
                <a${status == 1 ? ` href="groupClassDetailsUI?gcId=${v.id}"` : ''}>
                     <p>${status == 0 ? '已结束' : status == 1 ? '可预约' : '已满员'}</p>
                    <span>${v.realNum}</span>
                </a>
            </li>
        `.trim()
    },
    chooseDate(self, date) {
        if ($(self).hasClass('act')) return;
        this.date = date
        $(self).addClass('act').siblings().removeClass('act')
        this.pageNum = 1;
        this.fetch()
    },
    choose(self, type, id) {
        const _self = $(self)
        this.mask.removeClass('show')
        this.types.removeClass('act').eq(type).find('span').html(_self.html())
        if (_self.hasClass('act')) return;
        _self.addClass('act').siblings().removeClass('act')
        if (!type) {
            this.productId=null
            this.mask.eq(1).removeClass('init')
            this.mask.eq(1).html('<dt class="iconfont icon-favorite">选择课程</dt><dd class="iconfont icon-loading" style="text-align: center"></dd>')
            this.types.eq(1).find('span').html('课程选择')
        }
        this[type ? 'productId' : 'clubId'] = id;
        this.fetch()
    },
    fetch(more) {
        if (!more) {
            this.pageNum = 1;
            $.loading()
        }
        const self = this
        $.fetch({
            url: 'groupClasses/',
            data: {
                clubId: this.clubId,
                productId: this.productId,
                courseDate: this.date,
                pageSize: $.globalConfig.pageSize,
                pageNum: this.pageNum
            },
            cb(rs) {
                $.hide()
                more && self.loadMoreOption.el.hide()
                self.loadMoreOption.allow = rs.page * rs.limit < rs.total
                !more&&self.list.html('')
                if (rs.rows.length === 0) return
                self.pageNum++
                self.list.append(rs.rows.map(v => self.renderItem(v)).join(""))
            }
        })
    },
    loadMoreOption: {
        allow: false,
        el: $(".loadingMore"),
        cb() {
            action.fetch(1)
        }
    },
    init() {
        this.renderDays()
        $.loadMore(this.loadMoreOption)
    }
}

action.init()