const action = window.action = {
    mask: $(".time-picker"),
    notChooseTime: true,
    time: $('.chooseTime'),
    init() {
        $('.hour').append(hours.map(v => `<li>${v}</li>`).join(""));
        $('.minute').append(minutes.map(v => `<li>${v}</li>`).join(""));
        setTimeout(() => {
            new Translate($('.hour')[0])
            new Translate($('.minute')[0])
        }, 0)
        dataPicker.init(function (date) {
            //日期选择确认回调
            console.error(date)
        })
    },
    toggleMask(bool, confrim) {
        if (bool) {
            this.mask.addClass('show')
        } else {
            if (confrim) {
                const hour = this.mask.find('.act').eq(0).html()
                const mins = this.mask.find('.act').eq(1).html()
                if (hour >= 21 && mins > 30) {
                    return $.error('时间不得晚于21:30')
                }
                this.notChooseTime = false
                this.time.html(hour + ":" + mins)
                this.mask.removeClass('show')
            }
            this.mask.removeClass('show')
        }
    },
    confirm() {
        if (this.notChooseTime) {
            return $.error('请先选择时间')
        }
        //date :dataPicker.timeBox.html()
        //time : this.time.html()
        $.loading('正在预约')
        // $.fetch()
        setTimeout(function () {
            $.success('预约成功')
        }, 100000)
    }
}
action.init()
