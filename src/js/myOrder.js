const action = window.action = {
    pageNum: 1,
    xhr: null,
    list: $(".data"),
    operating(dom, id, flag) {
        common.operating(dom, id, flag,(rs)=> {
            $(dom).parents(".item").replaceWith(this.renderItem(rs))
        })
    },
    renderItem(v) {
        return `
				<div class="item">
					<a href="/myOrderInfo.html?orderId=${v.orderId}" class="flex between vstart">
						<div class="flex">
							<img src="${v.imageUrl}">
							<div class="pl10">
								<h1>${v.orderName}</h1>
								<p>${v.productType == 1 ? (v.number + '课时') : '&nbsp;'}</p>
								<h2 class="red">￥${v.price}</h2>
							</div>
						</div>
						<p class="${v.status == 0 ? 'red' : 'grey'}">${common.statsuDto(v.status)}</p>
					</a>
					${v.status == 0 ? `
						<div class="control flex end pt10">
							<span class="grey" onclick="action.operating(this,${v.orderId},0)">取消订单</span>
							<span class="red" onclick="action.operating(this,${v.orderId},1)">立即支付</span>
						</div>
					` : ''}
				</div>
			` + ``
    },
    fetch(more) {
        if (!more) {
            $.loading()
        }
        const self = this
        this.xhr = $.fetch({
            url: '/myorders',
            data: {
                pageSize: $.globalConfig.pageSize,
                pageNum: this.pageNum
            },
            cb(rs) {
                $.hide()
                more && self.loadMoreOption.el.hide()
                self.loadMoreOption.allow = rs.page * rs.limit < rs.total
                !more && self.list.html('')
                if (rs.rows.length === 0) return
                self.pageNum++
                self.list.append(rs.rows.map(v => self.renderItem(v)).join(""))
            }
        })
    },
    loadMoreOption: {
        allow: false,
        el: $(".loadingMore"),
        cb() {
            action.fetch(1)
        }
    },
    init() {
        this.fetch()
        $.loadMore(this.loadMoreOption)
    }
}
action.init()