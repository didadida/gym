$.loading()
var data = []
var slider = function (arr) {
	const sliderBox = $('figure')
	const slider = $('figure ul')
	const interval = 4000

	slider.css('width', arr.length * 100 + 'vw')
	slider.html(arr.map(v => `<li class="fl"><a href="${v.link}"><img src="${v.image}"></li></a>`).join(""))
	const len = arr.length
	if (len > 2) {
		let timer = null;
		let n = 0
		const winW = $(window).width()
		const move = () => {
			n = n == len ? 0 : n < 0 ? len - 1 : n
			slider.css({ 'transform': `translate3d(-${n * winW}px, 0, 0)` })
		}
		const loop = () => {
			timer = setInterval(function () {
				n++
				move()
			}, interval)
		}
		sliderBox[0].ontouchstart = function (e) {
			clearInterval(timer)
			let ev = e || event;
			const curX = slider.css('transform').match(/\d+(?=px,)/g)[0]
			const initX = ev.touches[0].pageX
			$(slider).addClass('notAni')
			sliderBox[0].ontouchmove = function (e) {
				ev = e || event;
				ev.preventDefault()
				const disX = ev.touches[0].pageX - curX
				slider.css('transform', `translate3d(${disX - initX}px, 0, 0)`)
			}
			sliderBox[0].ontouchend = function (e) {
				slider.removeClass('notAni')
				const disX = (e || event).changedTouches[0].pageX - initX
				if (disX > winW / 2) {
					n--
				} else if (disX < -winW / 2) {
					n++
				}
				move()
				loop()
			}
		}
		loop()
	}
}

var gps = function (lng, lat, time) {
	var map = new BMap.Map("map");
	map.centerAndZoom(new BMap.Point(lng, lat), 16);
	map.enableScrollWheelZoom(true);

	map.clearOverlays();
	var myIcon = new BMap.Icon("./images/maker.png", new BMap.Size(34, 47));
	var marker = new BMap.Marker({ lng, lat }, { icon: myIcon });
	map.addOverlay(marker);              // 将标注添加到地图中

	var geoc = new BMap.Geocoder();

	geoc.getLocation(new BMap.Point(lng, lat), function (rs) {
		const addComp = rs.addressComponents;
		const city = addComp.province === addComp.city ? addComp.city : addComp.province + addComp.city
		const addr = city + addComp.district + addComp.street + addComp.streetNumber
		const htm = `<p><b>当前位置：</b>${addr}</p><p><b>停车时长：</b>${time}</p>`
		$('.curpos').html(htm)
	});
}
var init = function () {
	if (data.filter(v => v).length >= 2) {
	const rs = data[1]
	slider(data[0])
	const seconds = Math.floor((Date.now() - new Date(rs.time).getTime()) / 1000)
	const time = Math.floor(seconds / 3600) + '小时' + Math.floor(seconds / 60 - Math.floor(seconds / 3600) * 60) + '分钟' + seconds % 60 + '秒'
	gps(rs.locLng, rs.locLat, time)
	$.hide(1)
	charge();
	};
}
$.fetch({ url: '/wx/gps/listBanner.php?pushed_type=2',type: 1 }).then(rs => {
	data[0] = rs;
	init()
})
 
$.fetch({ url: '/wx/gps/wxshareLocation.php?pushed_type=2',
    type: 1 
	}).then(rs => {
	data[1] = rs;
	init()
})

function charge() {
	$('#chargepage').attr('href','http://app.zhikaizaixian.com/ksd_card/wx/chargePage.php?pushed_type=2');
}
