const params = $.getUrlParams()
const action = window.action = {
    tabs: $('.tabs li'),
    pages: $('.page'),
    pageNum: 1,
    list: $('.pj ul'),
    moreButton: $('.more'),
    renderItem(v) {
        return `
            <li class="flex pt10 pb10">
                <img src="${v.imageUrl}">
                <dl class="pl10">
                    <dt><strong>${v.courseName}</strong><b>${v.createtime}</b></dt>
                    <!--5分制  star5 5分 star45 4.5分-->
                    <dd class="iconfont star${v.stars}"><b>${v.courseName}</b></dd>
                    <dd>content</dd>
                </dl>
            </li>
        `
    },
    fetch() {
        const self = this
        this.moreButton.toggleClass('moring')
        $.fetch({
            url: 'cmlist',
            data: {
                pageNum: this.pageNum,
                pageSize: $.globalConfig.pageSize,
                coachId: params.coachId,
                courseType: 1
            },
            cb(rs) {
                self.moreButton.toggleClass('moring')
                if (rs.total <= rs.limit * rs.page) {
                    self.moreButton.hide()
                }
                self.list.append(rs.rows.map(v => self.renderItem(v)).join(""))
            }
        })
    },
    init() {
        this.fetch()
        const self = this
        this.tabs.click(function () {
            $(this).addClass('act').siblings().removeClass('act')
            self.pages.hide()
            self.pages.eq($(this).index()).show()
        })
    }
}
action.init()