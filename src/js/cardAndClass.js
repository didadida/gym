const action = window.action = {
    md: $('#md'),
    types: $('#type'),
    time: $('#time'),
    list: $('.list'),
    clubId: null,//选择门店的id
    rangeType: 1,
    timeType: 1,
    pageNum: 1,
    xhr: null,
    renderMD() {
        const self = this
        $.fetch({
            url: '/clubDroplist',
            cb(rs) {
                if (!rs.clublist.length) return;
                self.md.html(`<option value="">门店</option>` + rs.clublist.map(v => `<option value="${v.id}">${v.name}</option>`).join(""))
            }
        })
    },
    renderItem(v) {
        return `
            <a href="/cardInfo.html?id=${v.id}" class="p10 flex">
                <div class="img-box"><img src="${v.imageUrl}"></div>
                <div class="pl10">
                    <h1><b>${v.productName}</b></h1>
                    <p>${v.changeType === 2 ? v.clongtime : v.longtime}天</p>
                    <p>可用权益：自助健身</p>
                    <p>有效期：${v.changeType === 2 ? v.clongtime : v.longtime}天</p>
                    <h2>${v.amount}</h2>
                </div>
            </a>
        `
    },
    chooseDate(self, date) {
        if ($(self).hasClass('act')) return;
        this.date = date
        $(self).addClass('act').siblings().removeClass('act')
        this.pageNum = 1;
        this.fetch()
    },
    fetch(more) {
        if (!more) {
            this.pageNum = 1;
            this.list.html(' ')
            $.loading()
        }
        const self = this
        this.xhr = $.fetch({
            url: '/membershipProdlist',
            data: {
                clubId: this.clubId,
                rangeType: this.rangeType,
                timeType: this.timeType,
                productName: this.productName,
                pageSize: $.globalConfig.pageSize,
                pageNum: this.pageNum
            },
            cb(rs) {
                $.hide()
                more && self.loadMoreOption.el.hide()
                self.loadMoreOption.allow = rs.page * rs.limit < rs.total
                !more && self.list.html('')
                if (rs.rows.length === 0) return
                self.pageNum++
                self.list.append(rs.rows.map(v => self.renderItem(v)).join(""))
            }
        })
    },
    search(){
        this.clubId = this.md.val()
        this.rangeType = this.types.val()
        this.timeType = this.time.val()
        this.fetch()
    },
    loadMoreOption: {
        allow: false,
        el: $(".loadingMore"),
        cb() {
            action.fetch(1)
        }
    },
    init() {
        this.renderMD()
        this.fetch()
        $.loadMore(this.loadMoreOption)
    }
}

action.init()