const action = window.action = {
    xx: '<span class="iconfont icon-xingxing"></span>',
    xx1: '<span class="iconfont icon-xingxing1"></span>',
    totalx: 5,
    xxWrap: $('em'),
    text: $('textarea'),
    unnameButton: $('input'),
    publish() {
        const evalutionInfo = this.text.val() //评价详情
        const star = this.xxWrap.find('.icon-xingxing').length; //评价星级
        const isUnName = this.unnameButton.val() //是否匿名
        if (!evalutionInfo) return $.error('评价内容不能为空')
        //$.fetch()
    },
    init() {
        const self = this
        this.xxWrap.on('click', 'span', function () {
            let htm = ''
            const len = $(this).index() + 1
            for (let i = 0; i < len; i++) {
                htm += self.xx
            }
            for (let i = 0; i < self.totalx - len; i++) {
                htm += self.xx1
            }
            self.xxWrap.html(htm)
        })
    }
}
action.init()