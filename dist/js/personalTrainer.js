'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var filters = {
    leftNum: null,
    rightNum: null,
    sex: null,
    coachNeed: null,
    coachLevel: null,
    serveTime: null
};
var action = window.action = {
    club: $('#md'),
    list: $('.ary'),
    condi: $('.condition'),
    condiType: $('.condition dl'),
    types: $('.condition h1'),
    ipts: $('input[type=number]'),
    pageNum: 1,
    index: 0,
    clubId: null,
    filters: filters,
    toggleMask: function toggleMask(flag, type) {
        if (flag) {
            //打开弹窗
            this.condi.show();
            this.condiType.hide();
            this.types.removeClass('act');
            this.condiType.eq(type).show();
            this.types.eq(type).addClass('act');
            this.index = type;
        } else {
            //关闭弹窗
            this.condi.hide();
            if (!type) return;
            var o = {};
            var condi = this.condiType.eq(this.index);
            this.condiType.eq(1 - this.index).find('.act').removeClass('act');
            condi.find('.act').each(function (_, v) {
                return o[v.dataset.key] = v.dataset.value;
            });
            this.index == 0 && this.ipts.each(function (_, v) {
                return v.value && (o[v.name] = v.value);
            });
            this.filters = _extends({}, filters, o);
            this.fetch();
        }
    },
    clearAct: function clearAct() {
        $('.info .act').removeClass('act');
    },
    clearInput: function clearInput() {
        this.ipts.val('');
    },
    reset: function reset() {
        this.clearInput();
        this.clearAct();
    },
    renderItem: function renderItem(v) {
        return '\n            <a href="/coachDetails.html?coachId=' + v.id + '" class="flex p10">\n                <div class="img-box"><img src="' + v.userImageDto + '"></div>\n                <dl class="pl10 pr10">\n                    <dt>' + v.accountNameDto + '</dt>\n                    <dd class="star' + v.stars + ' iconfont"></dd>\n                    <dd>' + v.introductionDto + '</dd>\n                </dl>\n                <p class="ksprice"><span>' + (v.priceDto || 0) + '</span></p>\n            </a>\n        ';
    },
    fetch: function fetch(more) {
        if (!more) {
            this.pageNum = 1;
            this.list.html(' ');
            $.loading();
        }
        var self = this;
        this.xhr = $.fetch({
            url: 'coachlist',
            // type:'post',
            data: _extends({
                clubId: this.clubId,
                pageSize: $.globalConfig.pageSize,
                pageNum: this.pageNum,
                popular: 0
            }, this.filters),
            cb: function cb(rs) {
                $.hide();
                if (more) {
                    self.loadMoreOption.el.hide();
                } else {
                    self.list.html('');
                    $(window).scrollTop(0);
                }
                self.loadMoreOption.allow = rs.page * rs.limit < rs.total;
                if (rs.rows.length === 0) return;
                rs.rows = rs.rows.concat(rs.rows, rs.rows, rs.rows);
                self.pageNum++;
                self.list.append(rs.rows.map(function (v) {
                    return self.renderItem(v);
                }).join(""));
            }
        });
    },

    loadMoreOption: {
        allow: false,
        el: $(".loadingMore"),
        cb: function cb() {
            action.fetch(1);
        }
    },
    selectMD: function selectMD() {
        this.clubId = this.club.val();
        this.fetch();
    },
    init: function init() {
        this.selectMD();
        $.loadMore(this.loadMoreOption);
        var that = this;
        $('.info').on('click', 'p', function () {
            that.clearInput();
            var self = $(this);
            if (self.hasClass('act')) return self.removeClass('act');
            self.addClass('act').siblings().removeClass('act');
        });
    }
};
action.init();