'use strict';

var action = window.action = {
    validDuration: 15 * 60 * 1000, //有效期15分钟
    endTime: 0,
    orderId: $.getUrlParams().orderId,
    time: null,
    timer: null,
    render: function render(rs) {
        $(".data").html('\n            <div class="flex between bt10 p10">\n                <div><h1>' + common.statsuDto(rs.status) + '</h1>' + (rs.status == 0 && rs.activityProductId ? '<span class="red iconfont icon-guanbi"><span class="time">14分59秒</span>后未支付，此订单将自动关闭</span>' : '') + '</div>\n                <img src="./images/' + (rs.status == 0 ? '6' : rs.status == 6 ? '4' : '5') + '.png" style="height: auto">\n            </div>\n            <div class="bt10 p10">\n                <div class="flex">\n                    <img src="' + rs.imageUrl + '">\n                    <div class="flex column between vstart pl10">\n                        <h1>' + rs.orderName + '</h1>\n                        <h2 class="grey">' + (rs.productType == 1 ? rs.number + '课时' : '') + '</h2>\n                    </div>\n                </div>\n                <div class="flex between pt10">\n                    <span class="grey">' + (rs.status == 0 ? '待付款' : rs.status == 6 ? '总金额' : '实付款') + '</span>\n                    <span class="red">\uFFE5' + rs.price + '</span>\n                </div>\n            </div>\n            <div class="bt10 p10 grey">\n                <div class="flex between">\n                    <span>' + (rs.status == 0 ? '下单' : rs.status == 6 ? '取消' : '支付') + '\u65F6\u95F4\uFF1A</span>\n                    <span>' + (rs.status == 0 ? rs.createTime : rs.status == 6 ? rs.cancelTime : rs.payTime) + '</span>\n                </div>\n                <div class="flex between">\n                    <span>\u8BA2\u5355\u7F16\u53F7\uFF1A</span>\n                    <span>' + rs.orderNo + '</span>\n                </div>\n            </div>\n            ' + (rs.status == 0 ? '\n                <div class="control flex end">\n                    <b class="grey" onclick="action.operating(this,' + this.orderId + ',0)">\u53D6\u6D88\u8BA2\u5355</b>\n                    <b class="red" onclick="action.operating(this,' + this.orderId + ',1)">\u7ACB\u5373\u652F\u4ED8</b>\n                </div>\n            ' : '') + '\n        ');
    },
    padleft: function padleft(n) {
        return (n < 10 ? '0' : '') + n;
    },
    cutDown: function cutDown() {
        var _this = this;

        if (this.endTime <= Date.now()) {
            this.time.parent().html("订单已关闭");
            $(".control").remove();
            clearTimeout(this.timer);
        } else {
            var t = parseInt((this.endTime - Date.now()) / 1000);
            var m = parseInt(t / 60);
            var s = parseInt(t % 60);
            this.time.html(this.padleft(m) + "分" + this.padleft(s) + "秒");
            this.timer = setTimeout(function () {
                return _this.cutDown();
            }, 1000);
        }
    },
    operating: function operating(dom, id, flag) {
        common.operating(dom, id, flag, function (rs) {
            location.reload();
        });
    },
    fetch: function fetch() {
        $.loading();
        var self = this;
        this.xhr = $.fetch({
            type: 'get',
            url: '/ordersDetails',
            data: {
                orderId: this.orderId
            },
            cb: function cb(rs) {
                $.hide();
                rs = rs || {};
                self.render(rs);
                if (rs.status == 0 && rs.activityProductId) {
                    self.time = $(".time");
                    self.endTime = new Date(rs.createTime.replace(/-/g, '/')).getTime() + self.validDuration;
                    self.cutDown();
                }
            }
        });
    },
    init: function init() {
        this.fetch();
    }
};
action.init();