"use strict";

var toast = window.toast = {
    el: $(".toast"),
    items: $(".toast-inner>div"),
    showClass: "show",
    hideClass: "none",
    toggle: function toggle(flag, index) {
        var _this = this;

        this.el.removeClass(flag ? this.hideClass : this.showClass);
        setTimeout(function () {
            _this.el.addClass(flag ? _this.showClass : _this.hideClass);
        }, 0);
        this.items.removeClass("show").eq(index).addClass(this.showClass);
    },
    success: function success() {
        this.toggle(1, 0);
    },
    fail: function fail() {
        this.toggle(1, 1);
    },
    payfail: function payfail() {
        this.toggle(1, 2);
    },
    close: function close(index) {
        action.orderId = null;
        this.toggle(0, index);
    }
};
var action = window.action = {
    wrapper: $("footer"),
    times: $(".time>span"),
    state: $(".time>i"),
    timer: null,
    go: $(".go"),
    side: $("aside"),
    title: $(".title"),
    list: $(".items"),
    data: { starttime: '', endtime: '' },
    activityProductId: null,
    orderId: null,
    padleft: function padleft(n) {
        return (n < 10 ? '0' : '') + n;
    },
    loop: function loop(startTime, endTime) {
        var _this2 = this;

        var now = Date.now();
        if (endTime < now) {
            this.wrapper.addClass("over");
            clearTimeout(this.timer);
        } else {
            var cur = void 0;
            if (startTime > now) {
                //还未开始
                // this.state.html("开始")
                this.wrapper.addClass("wait");
                cur = startTime;
            } else if (endTime >= now) {
                //正在抢购
                this.wrapper.removeClass("wait");
                // this.state.html("结束")
                cur = endTime;
            }

            var t = parseInt(Math.abs(cur - now) / 1000);
            var h = parseInt(t / 60 / 60);
            var m = parseInt(t / 60 % 60);
            var s = parseInt(t % 60);

            this.times.html(this.padleft(h) + ":" + this.padleft(m) + ":" + this.padleft(s));
            this.timer = setTimeout(function () {
                _this2.cutDown();
            }, 1000);
        }
    },
    cutDown: function cutDown() {
        if (!this.data.endtime || !this.data.starttime) return;
        var endTime = new Date(this.data.endtime.replace(/-/g, '/')).getTime();
        var startTime = new Date(this.data.starttime.replace(/-/g, '/')).getTime();
        this.loop(startTime, endTime);
    },
    renderTableRow: function renderTableRow(v) {
        return "<tr><td>" + v.purchaseNum + "\u8282</td><td>\uFFE5" + v.price + "</td><td>" + v.totalNum + "\u4EBA</td><td>" + v.leftNum + "\u4EBA</td></tr>";
    },
    renderPackageItem: function renderPackageItem(v, index) {
        return "<dd onclick=\"action.chooseItem(this," + index + "," + v.activityProductId + ")\">" + v.purchaseNum + "\u8282</dd>";
    },
    renderInfo: function renderInfo(price, surplus) {
        this.title.html("<h4>\uFFE5" + price + "</h4><h5>\u5E93\u5B58\uFF1A" + surplus + "\u4EF6</h5>");
        if (!surplus) {
            this.list.addClass("nomore");
        } else {
            this.list.removeClass("nomore");
        }
    },
    render: function render() {
        var _this3 = this;

        var s1 = '';
        var s2 = '';
        var data = this.data.rows || [];
        data.forEach(function (v, i) {
            s1 += _this3.renderTableRow(v);
            s2 += _this3.renderPackageItem(v, i);
        });
        $("main>h1").html("\n            \u5F00\u59CB\u65F6\u95F4\uFF1A" + this.data.starttime.replace("-", "年").replace("-", "月").replace(" ", "日 ") + "<br>\n            \u7ED3\u675F\u65F6\u95F4\uFF1A" + this.data.endtime.replace("-", "年").replace("-", "月").replace(" ", "日 ") + "\n        ");
        $("tbody").html(s1 || "<tr><td colspan='4'>暂无数据</td></tr>");
        this.list.html(s2 || "");

        if (!this.data.status) {
            this.wrapper.addClass("over");
            return;
        }
        var p = data.map(function (v) {
            return v.price;
        }).sort(function (a, b) {
            return b - a;
        });
        console.error(p);
        var i = p.length ? p[p.length - 1] + '-' + p[0] : 0;
        var j = data.reduce(function (sum, v) {
            return sum + v.leftNum;
        }, 0);
        this.renderInfo(i, j);
    },
    toggle: function toggle(flag) {
        this.side[flag ? 'addClass' : 'removeClass']("show");
    },
    chooseItem: function chooseItem(dom, index, activityProductId) {
        console.error(activityProductId);
        this.activityProductId = activityProductId;
        $(dom).addClass("active").siblings().removeClass("active");
        var item = this.data.rows[index];
        this.renderInfo(item.price, item.leftNum);
    },
    pay: function pay() {
        //购买
        if (!this.activityProductId) {
            return $.error("未选择商品");
        }
        var self = this;
        toast.close();
        data = {
            activityProductId: this.activityProductId
        };
        if (this.orderId) {
            data = {
                orderId: this.orderId
            };
        }
        $.loading("正在支付");
        $.fetch({
            type: 'get',
            url: 'actprolist',
            data: data,
            cb: function cb(rs) {
                //购买成功应该返回orderId,已方便用户前往详情页面
                self.orderId = rs.orderId;
                $.success('购买成功');
                //支付成功
                toast.success();
                self.toggle(0);
            },
            cb_error: function cb_error(rs) {
                //$.error('测试',1000) 第二个参数表示提示停留两秒
                // $.hide()
                //支付失败同样返回orderId,通过orderId重新支付，避免重新生成订单
                self.orderId = rs.orderId;
                //支付失败
                // toast.payfail()
                //没有抢到
                // toast.fail()
                $.error('购买失败');
                self.toggle(0);
            }
        });
    },
    goto: function goto() {
        //购买成功查看订单详情
        if (!this.orderId) {
            return $.error("订单不存在");
        }
        window.location.href = "/myOrder.html?orderId=" + this.orderId;
    },
    init: function init() {
        var self = this;
        $.loading();
        $.fetch({
            type: 'get',
            url: 'actprolist',
            data: {
                activityId: 1
            },
            cb: function cb(rs) {
                $.hide();
                $("main").show();
                rs && (self.data = rs);
                self.cutDown();
                self.render();
            }
        });
    }
};
action.init();
// toast.success()