'use strict';

$.loading();
var action = window.action = {
    el: $('.record ul'),
    pageNum: 1,
    get: function get() {
        var o = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        var self = this;
        $.fetch({
            type: 'get',
            url: 'admissionRecordPage',
            data: {
                pageNum: this.pageNum,
                pageSize: 10
            },
            cb: function cb(rs) {
                //无数据的时候
                if (rs.total == 0) {
                    $("#noneDiv").show();
                }
                $.hide();
                o.allow = true;
                o.el && o.el.hide();
                self.pageNum++;
                self.el.append(rs.rows.map(function (v) {
                    return ('\n                    <li class="pb10 pt10">\n                        <p>\u5165\u573A\u65F6\u95F4\uFF1A<span>' + v.beginTime + '</span></p>\n                        <p>\u79BB\u573A\u65F6\u95F4\uFF1A<span>' + (v.endTime || '暂未离场') + '</span></p>\n                        <div class="flex">\n                            <p class="flex1">\u624B\u724C\u53F7\uFF1A<span>' + (v.machineNo || '无') + '</span></p>\n                            <p class="flex1">\u5165\u573A\u72B6\u6001\uFF1A<span class="' + (v.status === 1 || v.status === 3 ? 'error' : 'ok') + '">' + (v.status === 1 ? '签到中' : v.status === 2 ? '签到成功' : v.status === 1 ? '签退中' : '签退成功') + '</span></p>\n                        </div>\n                    </li>\n                ').replace(/\s+/g, ' ');
                }));
                if (rs.total <= rs.limit * rs.page) {
                    window.onscroll = null;
                }
            }
        });
    },
    init: function init() {
        var self = this;
        $.loadMore({
            allow: true,
            el: $(".loadingMore"),
            cb: function cb() {
                self.get(this);
            }
        });
        this.get();
    }
};
action.init();