'use strict';

var action = window.action = {
    xx: '<span class="iconfont icon-xingxing"></span>',
    xx1: '<span class="iconfont icon-xingxing1"></span>',
    totalx: 5,
    xxWrap: $('em'),
    text: $('textarea'),
    unnameButton: $('input'),
    publish: function publish() {
        var evalutionInfo = this.text.val(); //评价详情
        var star = this.xxWrap.find('.icon-xingxing').length; //评价星级
        var isUnName = this.unnameButton.val(); //是否匿名
        if (!evalutionInfo) return $.error('评价内容不能为空');
        //$.fetch()
    },
    init: function init() {
        var self = this;
        this.xxWrap.on('click', 'span', function () {
            var htm = '';
            var len = $(this).index() + 1;
            for (var i = 0; i < len; i++) {
                htm += self.xx;
            }
            for (var _i = 0; _i < self.totalx - len; _i++) {
                htm += self.xx1;
            }
            self.xxWrap.html(htm);
        });
    }
};
action.init();