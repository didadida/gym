'use strict';

$.loading();
var action = window.action = {
    el: $('.record ul'),
    pageNum: 1,
    renderStatus: function renderStatus(v) {
        debugger;
        switch (v.parStatusDto) {
            case '待确认上课':
                return '<a onclick="action.confirm(this,' + v.id + ')">\u786E\u8BA4\u4E0A\u8BFE</a>';
            case '预约中':
                return '<a href="trainPlanUI?id=' + v.id + '">\u8BAD\u7EC3\u8BA1\u5212</a>';
            case '已上课':
                if (v.cmstatus != null) {
                    //0未评价1已评价
                    if (v.cmstatus == 0) {
                        return '<a href="trainContentUI?id=' + v.id + '">\u8BAD\u7EC3\u5185\u5BB9</a><a href="commentUI?id=' + v.id + '&courseType=1&courseName=' + v.courseNameDto + '">\u8BC4\u4EF7</a>';
                    } else {
                        return '<a href="trainContentUI?id=' + v.id + '">\u8BAD\u7EC3\u5185\u5BB9</a> \u5DF2\u8BC4\u4EF7';
                    }
                } else {
                    return '<a href="trainContentUI?id=' + v.id + '">\u8BAD\u7EC3\u5185\u5BB9</a><a href="commentUI?id=' + v.id + '&courseType=1&courseName=' + v.courseNameDto + '">\u8BC4\u4EF7</a>';
                }
            default:
                return '';
        }
    },
    renderItem: function renderItem(v) {
        return ('\n            <li>\n                <h1 class="p10 flex between">\n                    <b>' + v.courseNameDto + '</b>\n                </h1>\n                <div class="p10">\n                    <div class="flex">\n                        <p class="flex1">\u4E0A\u8BFE\u6559\u7EC3\uFF1A<span>' + v.coachnameDto + '</span></p>\n                        <p class="flex1">\u4E0A\u8BFE\u72B6\u6001\uFF1A<span' + (v.status === 1 ? ' class="ok"' : '') + '>' + v.parStatusDto + '</span></p>\n                    </div>\n                    <p>\u5F00\u59CB\u65F6\u95F4\uFF1A<span>' + (v.courseStartDto || '无') + '</span></p>\n                    <p>\u7ED3\u675F\u65F6\u95F4\uFF1A<span>' + (v.courseEndDto || '无') + '</span></p>\n                    <p>\u9884\u7EA6\u65F6\u95F4\uFF1A<span>' + (v.createtime || '无') + '</span></p>\n                    <p>\u786E\u8BA4\u4E0A\u8BFE\u65F6\u95F4\uFF1A<span>' + (v.endtime || '无') + '</span></p>\n                    <aside>' + this.renderStatus(v) + '</aside>\n                </div>\n            </li>\n        ').replace(/\s+/g, ' ');
    },
    get: function get() {
        var o = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        var self = this;
        $.fetch({
            type: 'get',
            url: 'classRecord',
            data: {
                pageNum: this.pageNum,
                pageSize: 10
            },
            cb: function cb(rs) {
                //无数据的时候
                if (rs.total == 0) {
                    $("#noneDiv").show();
                }
                $.hide();
                o.allow = true;
                o.el && o.el.hide();
                self.pageNum++;
                self.el.append(rs.rows.map(function (v) {
                    // console.error(self.renderItem(v))
                    return self.renderItem(v);
                }).join(""));
                if (rs.total <= rs.limit * rs.page) {
                    window.onscroll = null;
                }
            }
        });
    },
    confirm: function confirm(self, id) {
        var that = this;
        $.confirm({
            title: '是否确认上课?',
            ok: function ok() {
                $.fetch({
                    url: 'cfmCourse',
                    data: { id: id },
                    cb: function cb(rs) {
                        $.success('确认上课成功', null, function () {
                            $(self).parents('li').replaceWith(that.renderItem(rs));
                        });
                    }
                });
            }
        });
    },
    init: function init() {
        var self = this;
        $.loadMore({
            allow: true,
            el: $(".loadingMore"),
            cb: function cb() {
                self.get(this);
            }
        });
        this.get();
    }
};
action.init();