'use strict';

var action = window.action = {
    count: 1,
    countView: $('aside em'),
    price: 0,
    priceView: $('h4'),
    adjustment: function adjustment(n) {
        if (this.count == 1 && n == -1) return;
        this.count += n;
        this.countView.html(this.count);
        this.priceView.html((this.count * this.price).toFixed(2));
    },
    init: function init() {
        this.count = +this.countView.html();
        this.price = parseFloat(+this.priceView.html()).toFixed(2);
    },
    confirm: function confirm() {
        var cd = $("input[type='checkbox']:checked").val();
        if (!cd) return $.error('请同意包月协议');
        cd = $("input[type='radio']:checked").val();
        // if (!cd) return $.error('请选择场地')

        console.log('价格:', this.price);
        console.log('数量:', this.count);

        var domainName = document.domain;
        console.log("域名:" + domainName);
        var courseName = $('#courseName').val(); //课程名称

        $.loading('正在支付中');
        //支付
        $.fetch({
            url: 'toWeixinPay',
            data: {
                businessType: "1", //支付业务类型 1.私教课程购买2.团课购买3.购卡
                money: this.price * this.count,
                count: this.count,
                clubId: $("#clubId").val(),
                productId: $("#productId").val(),
                coachId: $("#coachId").val()
            },
            cb: function cb(rs) {
                console.log(rs);

                if (rs.money == 0) {
                    $.success('支付成功', null, function () {
                        window.location.href = "paySuccess?orderNo=" + rs.orderNo + "&money=" + rs.money + "&courseName=" + courseName;
                    });
                } else {

                    WeixinJSBridge.invoke('getBrandWCPayRequest', {
                        "appId": rs.payUrl.appId, "timeStamp": rs.payUrl.timeStamp, "nonceStr": rs.payUrl.nonceStr, "package": rs.payUrl.package, "signType": "MD5", "paySign": rs.payUrl.paySign
                    }, function (res) {
                        WeixinJSBridge.log(res.err_msg);
                        if (res.err_msg == "get_brand_wcpay_request:ok") {
                            window.location.href = "paySuccess?orderNo=" + rs.orderNo + "&money=" + rs.money + "&courseName=" + courseName;
                        } else if (res.err_msg == "get_brand_wcpay_request:cancel") {
                            $.error('微信支付取消');
                        } else {
                            console.log("订单NO:" + rs.orderNo);
                            $.error('微信支付失败!');
                        }
                    });
                }
            }
        });
    }
};
action.init();