"use strict";

var action = window.action = {
    pageNum: 1,
    xhr: null,
    list: $(".data"),
    operating: function operating(dom, id, flag) {
        var _this = this;

        common.operating(dom, id, flag, function (rs) {
            $(dom).parents(".item").replaceWith(_this.renderItem(rs));
        });
    },
    renderItem: function renderItem(v) {
        return "\n\t\t\t\t<div class=\"item\">\n\t\t\t\t\t<a href=\"/myOrderInfo.html?orderId=" + v.orderId + "\" class=\"flex between vstart\">\n\t\t\t\t\t\t<div class=\"flex\">\n\t\t\t\t\t\t\t<img src=\"" + v.imageUrl + "\">\n\t\t\t\t\t\t\t<div class=\"pl10\">\n\t\t\t\t\t\t\t\t<h1>" + v.orderName + "</h1>\n\t\t\t\t\t\t\t\t<p>" + (v.productType == 1 ? v.number + '课时' : '&nbsp;') + "</p>\n\t\t\t\t\t\t\t\t<h2 class=\"red\">\uFFE5" + v.price + "</h2>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<p class=\"" + (v.status == 0 ? 'red' : 'grey') + "\">" + common.statsuDto(v.status) + "</p>\n\t\t\t\t\t</a>\n\t\t\t\t\t" + (v.status == 0 ? "\n\t\t\t\t\t\t<div class=\"control flex end pt10\">\n\t\t\t\t\t\t\t<span class=\"grey\" onclick=\"action.operating(this," + v.orderId + ",0)\">\u53D6\u6D88\u8BA2\u5355</span>\n\t\t\t\t\t\t\t<span class=\"red\" onclick=\"action.operating(this," + v.orderId + ",1)\">\u7ACB\u5373\u652F\u4ED8</span>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t" : '') + "\n\t\t\t\t</div>\n\t\t\t" + "";
    },
    fetch: function fetch(more) {
        if (!more) {
            $.loading();
        }
        var self = this;
        this.xhr = $.fetch({
            url: '/myorders',
            data: {
                pageSize: $.globalConfig.pageSize,
                pageNum: this.pageNum
            },
            cb: function cb(rs) {
                $.hide();
                more && self.loadMoreOption.el.hide();
                self.loadMoreOption.allow = rs.page * rs.limit < rs.total;
                !more && self.list.html('');
                if (rs.rows.length === 0) return;
                self.pageNum++;
                self.list.append(rs.rows.map(function (v) {
                    return self.renderItem(v);
                }).join(""));
            }
        });
    },

    loadMoreOption: {
        allow: false,
        el: $(".loadingMore"),
        cb: function cb() {
            action.fetch(1);
        }
    },
    init: function init() {
        this.fetch();
        $.loadMore(this.loadMoreOption);
    }
};
action.init();