'use strict';

var params = $.getUrlParams();
var action = window.action = {
    tabs: $('.tabs li'),
    pages: $('.page'),
    pageNum: 1,
    list: $('.pj ul'),
    moreButton: $('.more'),
    renderItem: function renderItem(v) {
        return '\n            <li class="flex pt10 pb10">\n                <img src="' + v.imageUrl + '">\n                <dl class="pl10">\n                    <dt><strong>' + v.courseName + '</strong><b>' + v.createtime + '</b></dt>\n                    <!--5\u5206\u5236  star5 5\u5206 star45 4.5\u5206-->\n                    <dd class="iconfont star' + v.stars + '"><b>' + v.courseName + '</b></dd>\n                    <dd>content</dd>\n                </dl>\n            </li>\n        ';
    },
    fetch: function fetch() {
        var self = this;
        this.moreButton.toggleClass('moring');
        $.fetch({
            url: 'cmlist',
            data: {
                pageNum: this.pageNum,
                pageSize: $.globalConfig.pageSize,
                coachId: params.coachId,
                courseType: 1
            },
            cb: function cb(rs) {
                self.moreButton.toggleClass('moring');
                if (rs.total <= rs.limit * rs.page) {
                    self.moreButton.hide();
                }
                self.list.append(rs.rows.map(function (v) {
                    return self.renderItem(v);
                }).join(""));
            }
        });
    },
    init: function init() {
        this.fetch();
        var self = this;
        this.tabs.click(function () {
            $(this).addClass('act').siblings().removeClass('act');
            self.pages.hide();
            self.pages.eq($(this).index()).show();
        });
    }
};
action.init();