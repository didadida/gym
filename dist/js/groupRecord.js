'use strict';

$.loading();
var action = window.action = {
    el: $('.record ul'),
    pageNum: 1,
    renderStatus: function renderStatus(v, flag) {
        switch (v.status) {
            case 0:
                return flag ? '<a href="cancelGroupCourseUI?id=' + v.id + '">\u53D6\u6D88\u9884\u7EA6</a>' : '';
            case 1:
                if (v.cmstatus != null) {
                    //0未评价1已评价
                    if (v.cmstatus == 0) {
                        return '<a href="commentUI?id=' + v.id + '&courseType=2&courseName=' + v.courseNameDto + '">\u8BC4\u4EF7</a>';
                    } else {
                        return '\u5DF2\u8BC4\u4EF7';
                    }
                } else {
                    return '<a href="commentUI?id=' + v.id + '&courseType=2&courseName=' + v.courseNameDto + '">\u8BC4\u4EF7</a>';
                }

            default:
                return '';
        }
    },
    renderItem: function renderItem(v) {
        var flag = Date.now() < new Date(v.courseStartDto.replace(/-/g, '/')).getTime();
        return ('<li>\n                <h1 class="p10 flex between">\n                    <b>' + v.courseNameDto + '</b>\n                </h1>\n                <div class="p10">\n                    <div class="flex">\n                        <p class="flex1">\u4E0A\u8BFE\u6559\u7EC3\uFF1A<span>' + v.coachnameDto + '</span></p>\n                        <p class="flex1">\u4E0A\u8BFE\u72B6\u6001\uFF1A<span>' + (v.status == 0 && !flag ? '待确认上课' : v.parStatusDto) + '</span></p>\n                    </div>\n                    <p>\u4E0A\u8BFE\u65F6\u95F4\uFF1A<span>' + (v.courseStartDto || '无') + '</span></p>\n                    <p>\u9884\u7EA6\u65F6\u95F4\uFF1A<span>' + (v.createtime || '无') + '</span></p>\n                    <p>\u786E\u8BA4\u4E0A\u8BFE\u65F6\u95F4\uFF1A<span>' + (v.endtime || '无') + '</span></p>\n                    <div class="flex">\n                        <p class="flex1">\u4EF7\u683C\uFF1A<span>' + v.priceDto + '</span></p>\n                        <p class="flex1">\u4E0A\u8BFE\u4EBA\u6570\uFF1A<span>' + (v.currentPsNumDto + '/' + v.personNumDto) + '</span></p>\n                    </div>\n                    <aside>' + this.renderStatus(v, flag) + '</aside>\n                </div>\n            </li>').replace(/\s+/g, ' ');
    },
    get: function get() {
        var o = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        var self = this;
        $.fetch({
            type: 'get',
            url: 'groupClassRecord',
            data: {
                pageNum: this.pageNum,
                pageSize: $.globalConfig.pageSize
            },
            cb: function cb(rs) {
                //无数据的时候
                if (rs.total == 0) {
                    $("#noneDiv").show();
                }
                $.hide();
                o.allow = true;
                o.el && o.el.hide();
                self.pageNum++;
                self.el.append(rs.rows.map(function (v) {
                    return self.renderItem(v);
                }).join(""));
                if (rs.total <= rs.limit * rs.page) {
                    window.onscroll = null;
                }
            }
        });
    },
    init: function init() {
        var self = this;
        $.loadMore({
            allow: true,
            el: $(".loadingMore"),
            cb: function cb() {
                self.get(this);
            }
        });
        this.get();
    }
};
action.init();