'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Date.prototype.toJSON = function () {
    function addZ(n) {
        return (n < 10 ? '0' : '') + n;
    }

    return function () {
        return this.getFullYear() + '-' + addZ(this.getMonth() + 1) + '-' + addZ(this.getDate());
    };
}();

var dataPicker = window.dataPicker = {
    timeBox: $('.timebox'),
    picker: $(".date-picker"),
    year: $('#date-year'),
    month: $('#date-month'),
    day: $('#date-day'),
    body: $('.date-body'),
    dateBox: $('.date-box'),
    yearBox: $('.year-box'),
    monthBox: $('.month-box'),
    prev: $('#prev'),
    cols: 6 * 7,
    oneDay: 1000 * 60 * 60 * 24,
    now: new Date(),
    lastAct: null,
    cb: undefined,
    show: function show() {
        this.picker.show();
    },
    hide: function hide() {
        this.picker.hide();
    },
    getCurShowDate: function getCurShowDate() {
        return new Date(this.year.html() + '-' + this.padZero(this.month.html()) + '-' + this.padZero(this.day.html()));
    },
    confrim: function confrim() {
        var d = this.getCurShowDate();
        if (d.getTime() < this.now.getTime()) {
            return $.error('日期必须大于等于今日');
        }
        this.timeBox.html(d.toJSON());
        this.hide();
        if (d.getTime() === this.now.getTime()) {
            this.prev.addClass('disabled');
        } else {
            this.prev.removeClass('disabled');
        }
        this.cb && this.cb(d.toJSON());
    },
    showBox: function showBox(type) {
        this.dateBox.removeClass('entry');
        this.yearBox.removeClass('entry');
        this.monthBox.removeClass('entry');
        if (type === 1) {
            this.yearBox.addClass('entry');
        } else if (type === 2) {
            this.monthBox.addClass('entry');
        } else {
            this.dateBox.addClass('entry');
        }
    },
    padZero: function padZero(n) {
        return n < 10 ? "0" + n : n;
    },
    renderYM: function renderYM(o, t) {
        o.find('.act').removeClass('act');
        o.children().each(function (i, v) {
            if (v.innerText == Number(t)) {
                v.className = 'act';
            }
        });
    },
    renderBody: function renderBody(y, m, d, now) {
        var _this2 = this;

        m = this.padZero(m);
        d = this.padZero(d);
        var day = new Date(new Date(y + "-" + m + "-01") - this.oneDay);
        day = new Date(new Date(day - day.getDay() * this.oneDay).setHours(8, 0, 0, 0)).getTime();
        this.body.html(new Array(this.cols).fill(0).map(function (_, i) {
            var _d = new Date(day + i * _this2.oneDay);
            var t = _d.getTime();
            var cls = 'class="';
            if (t < now.getTime()) {
                cls += 'disabled';
            } else if (t == now.getTime()) {
                cls += 'iconfont today';
            }
            if (_d.getMonth() + 1 != m) {
                cls += ' not';
            } else if (_d.getDate() == d) {
                cls += ' act';
            }
            cls += '"';
            return '<li data-timestamp="' + t + '" ' + cls + '>' + _d.getDate() + '</li>';
        }).join(""));
        this.renderYM(this.monthBox, m);
        this.renderYM(this.yearBox, y);
    },
    render: function render(date, isRender) {
        date = date || new Date();
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        var d = date.getDate();
        this.year.html(y);
        this.month.html(m);
        this.day.html(d);
        if (isRender) {
            this.renderBody(y, m, d, this.now);
        }
    },
    reInit: function reInit() {
        this.prev.addClass('disabled');
        this.now.setHours(8, 0, 0, 0);
        this.timeBox.html(this.now.toJSON());
        this.render(this.now, 1);
        var m = this.now.getMonth() + 1;
        var y = this.now.getFullYear();
        var sy = y - 3;
        this.monthBox.html(new Array(12).fill(0).map(function (_, i) {
            return '<li class="' + (i + 1 === m ? 'act' : '') + '">' + (i + 1) + '</li>';
        }).join(""));
        this.yearBox.html(new Array(12).fill(0).map(function (_, i) {
            return '<li class="' + (sy + i === y ? 'act' : '') + '">' + (sy + i) + '</li>';
        }).join(""));
        $('.date-header').html(['日', '一', '二', '三', '四', '五', '六'].map(function (v) {
            return '<li>' + v + '</li>';
        }).join(""));
        this.cb && this.cb(this.now.toJSON());
    },
    init: function init(cb) {
        this.cb = cb;
        var self = this;
        this.reInit();
        this.body.on('touchstart', function (e) {
            var src = e.target;
            var _this = $(src);
            if (_this.hasClass('disabled')) return;
            var n = void 0;
            if (_this.hasClass('not')) {
                n = 1;
            } else {
                _this.addClass('act').siblings(".act").removeClass('act');
                n = 0;
            }
            self.render(new Date(Number(src.dataset.timestamp)), n);
        });
        this.monthBox.on('touchstart', function (e) {
            self.bindEvt(e, 1);
        });
        this.yearBox.on('touchstart', function (e) {
            self.bindEvt(e, 0);
        });
    },
    bindEvt: function bindEvt(e, b) {
        var src = e.target;
        var _this = $(src);
        var self = this;
        this.prev.addClass('disabled');
        if (_this.hasClass('act')) return;
        $(src).addClass('act').siblings(".act").removeClass('act');
        $(src).parent().removeClass('entry');
        if (b) {
            self.render(new Date(self.year.html() + "-" + self.padZero(_this.html()) + "-" + self.padZero(self.day.html())), 1);
            self.dateBox.addClass('entry');
        } else {
            self.render(new Date(_this.html() + "-" + self.padZero(self.month.html()) + "-" + self.padZero(self.day.html())), 1);
            self.monthBox.addClass('entry');
        }
    },
    goto: function goto(n) {
        var d = new Date(new Date(this.timeBox.html()).getTime() + n * this.oneDay);
        this.timeBox.html(d.toJSON());
        this.render(d, 1);
        this.cb && this.cb(d.toJSON());
        if (d.getTime() === this.now.getTime()) {
            this.prev.addClass('disabled');
        } else {
            this.prev.removeClass('disabled');
        }
    },
    fetch: function fetch(date) {
        console.error(date);
        // $.loading('正在获取预约表')
        /*$.fetch({
            url: '',
            data: {}
        }).then(rs => {
          })*/
    }
};

var hours = ["07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21"];
var minutes = new Array(6).fill(0).map(function (_, i) {
    return i * 10 < 10 ? "0" + i : String(i * 10);
});

var Translate = function () {
    function Translate(src) {
        _classCallCheck(this, Translate);

        this.FH = $(src).height();
        this.IH = $(src).children("li:first-child").height();
        this.disItem = 2;
        this.maxH = this.FH - this.IH * (this.disItem + 1);
        this.curH = this.lastY = this.curY = 0;
        this.src = src;
        this.lastItem = null;
        $(src).parents(".time-picker").on('touchmove', function (e) {
            e.preventDefault();
        }, false);
        this.init();
        src.addEventListener('touchstart', this.start.bind(this), false);
        src.addEventListener('touchmove', this.move.bind(this), false);
        src.addEventListener('touchend', this.end.bind(this), false);
    }

    _createClass(Translate, [{
        key: 'init',
        value: function init() {
            this.lastItem = $(this.src.children[0]);
            this.curY = this.disItem * this.IH;
            this.end();
        }
    }, {
        key: 'start',
        value: function start(e) {
            e.preventDefault();
            this.src.style.transitionDuration = "0s";
            this.curH = Number.parseInt(getComputedStyle(this.src, false).transform.split(",")[5]);
            this.lastY = e.changedTouches[0].pageY;
        }
    }, {
        key: 'move',
        value: function move(e) {
            e.preventDefault();
            var disY = this.curH + e.changedTouches[0].pageY - this.lastY;
            if (disY <= -this.maxH) {
                disY = -this.maxH;
            } else if (disY > this.IH * this.disItem) {
                disY = this.IH * this.disItem;
            }
            this.curY = disY;
            var item = $(this.src).find('li').eq(Math.round(this.disItem - disY / this.IH));
            item.addClass("act");
            if (item[0] !== this.lastItem[0]) {
                this.lastItem && this.lastItem.removeClass('act');
                this.lastItem = item;
            }
            this.src.style.transform = 'matrix(1, 0, 0, 1, 0, ' + disY + ')';
        }
    }, {
        key: 'end',
        value: function end(e) {
            this.src.style.transitionDuration = "0.3s";
            var dis = this.curY % this.IH;
            this.curY -= dis;
            var item = $(this.src).find('li').eq(Math.round(this.disItem - this.curY / this.IH));
            item.addClass("act");
            if (item[0] !== this.lastItem[0]) {
                this.lastItem && this.lastItem.removeClass('act');
                this.lastItem = item;
            }
            this.src.style.transform = 'matrix(1, 0, 0, 1, 0, ' + this.curY + ')';
        }
    }]);

    return Translate;
}();