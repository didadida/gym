'use strict';

var action = window.action = {
    money: $('h4 span'),
    initMoney: 0,
    calcMoney: function calcMoney(src) {
        var m = parseInt($('option:checked').text());
        if (m >= this.initMoney) {
            $.error('优惠券金额过大');
            $(src).val("");
            m = 0;
        }
        this.money.html(this.initMoney - m);
    },
    init: function init() {
        this.initMoney = this.money.html().replace(/元/g, "");
    },
    pay: function pay() {
        var persons = $("input[type='radio']:checked").val(); //人数
        var id = $('option:checked').val() || null; //优惠券ID
        var gcId = $('#gcId').val(); //团课记录ID
        var courseName = $('#courseName').val(); //课程名称
        var money = this.initMoney; //价格

        console.log("价格:" + money);

        var domainName = document.domain;
        console.log("域名:" + domainName);

        $.loading('正在支付中');
        //支付
        $.fetch({
            url: 'toWeixinPay',
            data: {
                businessType: "2", //支付业务类型 1.私教课程购买2.团课购买3.购卡
                gcId: gcId,
                money: this.initMoney
            },
            cb: function cb(rs) {
                console.log(rs);

                if (rs.money == 0) {
                    $.success('支付成功', null, function () {
                        window.location.href = "paySuccess?orderNo=" + rs.orderNo + "&money=" + rs.money + "&courseName=" + courseName;
                    });
                } else {

                    WeixinJSBridge.invoke('getBrandWCPayRequest', {
                        "appId": rs.payUrl.appId, "timeStamp": rs.payUrl.timeStamp, "nonceStr": rs.payUrl.nonceStr, "package": rs.payUrl.package, "signType": "MD5", "paySign": rs.payUrl.paySign
                    }, function (res) {
                        WeixinJSBridge.log(res.err_msg);
                        if (res.err_msg == "get_brand_wcpay_request:ok") {
                            window.location.href = "paySuccess?orderNo=" + rs.orderNo + "&money=" + rs.money + "&courseName=" + courseName;
                        } else if (res.err_msg == "get_brand_wcpay_request:cancel") {
                            $.error('微信支付取消');
                        } else {
                            console.log("订单NO:" + rs.orderNo);
                            $.error('微信支付失败!');
                        }
                    });
                }
            }
        });
    }
};
action.init();