'use strict';

$('.input-date').datePicker({
    type: 'date-time',
    titleDisplay: false
});
var action = window.action = {
    compnayname: $("#compnayname"),
    contactname: $("#contactname"),
    number: $("#number"),
    classname: $("#classname"),
    timestart: $("#timestart"),
    duration: $("#duration"),
    teachername: $("#teachername"),
    checkItems: function checkItems() {
        var compnayname = this.compnayname.val();
        var contactname = this.contactname.val();
        var number = this.number.val();
        var classname = this.classname.val();
        var timestart = this.timestart.val();
        var duration = this.duration.val();
        var teachername = this.teachername.val();

        // if (compnayname == "") {
        //     return "请输入公司名称"
        // }
        // if (contactname == "") {
        //     return "请输入公司联系人名称"
        // }
        // //只匹配了手机号码，如果要匹配电话号码，请在网上找一段正则即可
        // if (!/^(?:\+?86)?1(?:3(?:4[^9\D]|[5-9]\d)|5[^3-6\D]\d|8[23478]\d|(?:78|98)\d)\d{7}$/.test(number)) {
        //     return "请输入正确的手机电话号码"
        // }
        // if (classname == "") {
        //     return "请输入课程名称"
        // }
        // if (timestart == "") {
        //     return "请选择包场开始日期时间"
        // }
        // if (duration == "") {
        //     return "请输入包场时长"
        // }

        return {
            compnayname: compnayname,
            contactname: contactname,
            number: number,
            classname: classname,
            timestart: timestart,
            duration: duration,
            teachername: teachername
        };
    },
    submit: function submit() {
        var result = this.checkItems();
        // if (typeof result === 'string') {
        //     return $.error(result)
        // }
        console.error(result);
        $.fetch({
            url: '/test',
            type: 'post',
            data: result,
            cb: function cb(rs) {
                $.success('提交成功，稍后会有同事联系您');
            }
        });
    }
};