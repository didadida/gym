'use strict';

var action = window.action = {
    md: $('#md'),
    types: $('#type'),
    time: $('#time'),
    list: $('.list'),
    clubId: null, //选择门店的id
    rangeType: 1,
    timeType: 1,
    pageNum: 1,
    xhr: null,
    renderMD: function renderMD() {
        var self = this;
        $.fetch({
            url: '/clubDroplist',
            cb: function cb(rs) {
                if (!rs.clublist.length) return;
                self.md.html('<option value="">\u95E8\u5E97</option>' + rs.clublist.map(function (v) {
                    return '<option value="' + v.id + '">' + v.name + '</option>';
                }).join(""));
            }
        });
    },
    renderItem: function renderItem(v) {
        return '\n            <a href="/cardInfo.html?id=' + v.id + '" class="p10 flex">\n                <div class="img-box"><img src="' + v.imageUrl + '"></div>\n                <div class="pl10">\n                    <h1><b>' + v.productName + '</b></h1>\n                    <p>' + (v.changeType === 2 ? v.clongtime : v.longtime) + '\u5929</p>\n                    <p>\u53EF\u7528\u6743\u76CA\uFF1A\u81EA\u52A9\u5065\u8EAB</p>\n                    <p>\u6709\u6548\u671F\uFF1A' + (v.changeType === 2 ? v.clongtime : v.longtime) + '\u5929</p>\n                    <h2>' + v.amount + '</h2>\n                </div>\n            </a>\n        ';
    },
    chooseDate: function chooseDate(self, date) {
        if ($(self).hasClass('act')) return;
        this.date = date;
        $(self).addClass('act').siblings().removeClass('act');
        this.pageNum = 1;
        this.fetch();
    },
    fetch: function fetch(more) {
        if (!more) {
            this.pageNum = 1;
            this.list.html(' ');
            $.loading();
        }
        var self = this;
        this.xhr = $.fetch({
            url: '/membershipProdlist',
            data: {
                clubId: this.clubId,
                rangeType: this.rangeType,
                timeType: this.timeType,
                productName: this.productName,
                pageSize: $.globalConfig.pageSize,
                pageNum: this.pageNum
            },
            cb: function cb(rs) {
                $.hide();
                more && self.loadMoreOption.el.hide();
                self.loadMoreOption.allow = rs.page * rs.limit < rs.total;
                !more && self.list.html('');
                if (rs.rows.length === 0) return;
                self.pageNum++;
                self.list.append(rs.rows.map(function (v) {
                    return self.renderItem(v);
                }).join(""));
            }
        });
    },
    search: function search() {
        this.clubId = this.md.val();
        this.rangeType = this.types.val();
        this.timeType = this.time.val();
        this.fetch();
    },

    loadMoreOption: {
        allow: false,
        el: $(".loadingMore"),
        cb: function cb() {
            action.fetch(1);
        }
    },
    init: function init() {
        this.renderMD();
        this.fetch();
        $.loadMore(this.loadMoreOption);
    }
};

action.init();