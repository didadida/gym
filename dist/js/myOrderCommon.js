'use strict';

var common = {
    statsuDto: function statsuDto(status) {
        switch (+status) {
            case 0:
                return '\u5F85\u652F\u4ED8';
            // case 1:
            // case 3:
            // case 4:
            //     return `支付成功`
            // case 2:
            //     return `交易失败关闭`
            // case 5:
            //     return `定金支付`
            case 6:
                return '\u5DF2\u53D6\u6D88';
            // case 7:
            //     return `已退款`
            default:
                return '\u5DF2\u5B8C\u6210';
        }
    },
    operating: function operating(dom, id, flag, _cb) {
        //取消订单
        var k = flag ? '支付' : '取消';
        var url = flag ? '支付地址' : 'wxcancelOrder'; //替换地址
        $.loading("正在" + k + "订单");
        var self = this;
        $.fetch({
            url: url,
            data: {
                orderId: id
            },
            cb: function cb(rs) {
                $.success("订单" + k + "成功", null, function () {
                    _cb(rs);
                });
            }
        });
    }
};